/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H
#define C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief 提供2D绘制功能
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_typography.h
 *
 * @brief 定义绘制模块中排版相关的函数
 *
 * @since 8
 * @version 1.0
 */

#include "drawing_canvas.h"
#include "drawing_color.h"
#include "drawing_text_declaration.h"
#include "drawing_types.h"

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 文字方向
 */
enum OH_Drawing_TextDirection {
    /** 方向：从右到左 */
    TEXT_DIRECTION_RTL,
    /** 方向：从左到右 */
    TEXT_DIRECTION_LTR,
};

/**
 * @brief 文字对齐方式
 */
enum OH_Drawing_TextAlign {
    /** 左对齐 */
    TEXT_ALIGN_LEFT,
    /** 右对齐 */
    TEXT_ALIGN_RIGHT,
    /** 居中对齐 */
    TEXT_ALIGN_CENTER,
    /**
     * 两端对齐，即紧靠左和右边缘，中间单词空隙由空格填充
     * 最后一行除外
     */
    TEXT_ALIGN_JUSTIFY,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_START和TEXT_ALIGN_LEFT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_START和TEXT_ALIGN_RIGHT相同。
     */
    TEXT_ALIGN_START,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_END和TEXT_ALIGN_RIGHT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_END和TEXT_ALIGN_LEFT相同。
     */
    TEXT_ALIGN_END,
};

/**
 * @brief 字重
 */
enum OH_Drawing_FontWeight {
    /** 字重为thin */
    FONT_WEIGHT_100,
    /** 字重为extra-light */
    FONT_WEIGHT_200,
    /** 字重为light */
    FONT_WEIGHT_300,
    /** 字重为normal/regular */
    FONT_WEIGHT_400,
    /** 字重为medium */
    FONT_WEIGHT_500,
    /** 字重为semi-bold */
    FONT_WEIGHT_600,
    /** 字重为bold */
    FONT_WEIGHT_700,
    /** 字重为extra-bold */
    FONT_WEIGHT_800,
    /** 字重为black */
    FONT_WEIGHT_900,
};

/**
 * @brief 基线位置
 */
enum OH_Drawing_TextBaseline {
    /** 用于表音文字，基线在中间偏下的位置 */
    TEXT_BASELINE_ALPHABETIC,
    /** 用于表意文字，基线位于底部 */
    TEXT_BASELINE_IDEOGRAPHIC,
};

/**
 * @brief 文本装饰
 */
enum OH_Drawing_TextDecoration {
    /** 无装饰 */
    TEXT_DECORATION_NONE = 0x0,
    /** 下划线 */
    TEXT_DECORATION_UNDERLINE = 0x1,
    /** 上划线 */
    TEXT_DECORATION_OVERLINE = 0x2,
    /** 删除线 */
    TEXT_DECORATION_LINE_THROUGH = 0x4,
};

/**
 * @brief 区分字体是否为斜体
 */
enum OH_Drawing_FontStyle {
    /** 非斜体 */
    FONT_STYLE_NORMAL,
    /** 斜体 */
    FONT_STYLE_ITALIC,
};

/**
 * @brief 创建OH_Drawing_TypographyStyle
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TypographyStyle对象的指针
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyStyle* OH_Drawing_CreateTypographyStyle(void);

/**
 * @brief 释放被OH_Drawing_TypographyStyle对象占据的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 设置文本方向
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针
 * @param int OH_Drawing_TextDirection枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextDirection(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextDirection */);

/**
 * @brief 设置文本对齐方式
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针
 * @param int OH_Drawing_TextAlign枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextAlign(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextAlign */);

/**
 * @brief 设置文本最大行数
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针
 * @param int 最大行数
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextMaxLines(OH_Drawing_TypographyStyle*, int /* maxLines */);

/**
 * @brief 创建OH_Drawing_TextStyle
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TextStyle对象的指针
 * @since 8
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_CreateTextStyle(void);

/**
 * @brief 释放被OH_Drawing_TextStyle对象占据的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTextStyle(OH_Drawing_TextStyle*);

/**
 * @brief 设置文本颜色
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param uint32_t 颜色
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置字号
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param double 字号
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontSize(OH_Drawing_TextStyle*, double /* fontSize */);

/**
 * @brief 设置字重
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param int OH_Drawing_FontWeight枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontWeight(OH_Drawing_TextStyle*, int /* OH_Drawing_FontWeight */);

/**
 * @brief 设置字体基线位置
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param int OH_Drawing_TextBaseline枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBaseLine(OH_Drawing_TextStyle*, int /* OH_Drawing_TextBaseline */);

/**
 * @brief 设置装饰
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param int OH_Drawing_TextDecoration枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief 设置装饰颜色
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param uint32_t 颜色
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置字体高度
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param double 字体高度
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontHeight(OH_Drawing_TextStyle*, double /* fontHeight */);

/**
 * @brief 设置字体类型
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param int 字体名称数量
 * @param char 指向字体类型的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontFamilies(OH_Drawing_TextStyle*,
    int /* fontFamiliesNumber */, const char* fontFamilies[]);

/**
 * @brief 设置字体风格
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param int OH_Drawing_FontStyle枚举类型
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyle(OH_Drawing_TextStyle*, int /* OH_Drawing_FontStyle */);

/**
 * @brief 设置语言区域
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @param char 语言区域，数据类型为指向char的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLocale(OH_Drawing_TextStyle*, const char*);

/**
 * @brief 创建指向OH_Drawing_TypographyCreate对象的指针
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle的指针
 * @param OH_Drawing_FontCollection 指向OH_Drawing_FontCollection的指针
 * @return 指向新创建的OH_Drawing_TypographyCreate对象的指针
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyCreate* OH_Drawing_CreateTypographyHandler(OH_Drawing_TypographyStyle*,
    OH_Drawing_FontCollection*);

/**
 * @brief 释放被OH_Drawing_TypographyCreate对象占据的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyHandler(OH_Drawing_TypographyCreate*);

/**
 * @brief 设置排版风格
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPushStyle(OH_Drawing_TypographyCreate*, OH_Drawing_TextStyle*);

/**
 * @brief 设置文本内容
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针
 * @param char 指向文本内容的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddText(OH_Drawing_TypographyCreate*, const char*);

/**
 * @brief 排版弹出
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPop(OH_Drawing_TypographyCreate*);

/**
 * @brief 创建OH_Drawing_Typography
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针
 * @return 指向OH_Drawing_Typography对象的指针
 * @since 8
 * @version 1.0
 */
OH_Drawing_Typography* OH_Drawing_CreateTypography(OH_Drawing_TypographyCreate*);

/**
 * @brief 释放OH_Drawing_Typography对象占据的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypography(OH_Drawing_Typography*);

/**
 * @brief 排版布局
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针
 * @param double 文本最大宽度
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyLayout(OH_Drawing_Typography*, double /* maxWidth */);

/**
 * @brief 显示文本
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针
 * @param OH_Drawing_Canvas 指向OH_Drawing_Canvas对象的指针
 * @param double x坐标
 * @param double y坐标
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyPaint(OH_Drawing_Typography*, OH_Drawing_Canvas*,
    double /* potisionX */, double /* potisionY */);

#ifdef __cplusplus
}
#endif
/** @} */
#endif