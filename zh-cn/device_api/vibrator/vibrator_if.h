/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 马达
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 服务获取驱动对象或者代理后，马达服务启动或停止振动。
 * 通过驱动程序对象或代理提供使用功能。
 *
 * @since 2.2
 */

/**
 * @file vibrator_if.h
 *
 * @brief 定义马达数据结构，包括马达模式和效果振动。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef VIBRATOR_IF_H
#define VIBRATOR_IF_H

#include <stdint.h>
#include "vibrator_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

struct VibratorInterface {
    /**
     * @brief 控制马达以执行给定持续时间的一次性振动。
     *
     * 单次振动与周期振动相互排斥。在执行一次性振动之前，需退出周期性振动。
     *
     * @param duration 指示一次性振动的持续时间，以毫秒为单位。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*StartOnce)(uint32_t duration);

    /**
     * @brief 控制马达以预置效果执行周期性振动。
     *
     * 单次振动与周期振动相互排斥。在执行一次性振动之前，需退出周期性振动。
     *
     * @param effectType 指向指示预置效果类型的指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Start)(const char *effectType);

    /**
     * @brief 停止马达振动。
     *
     * 马达启动前，必须在任何模式下停止振动。此功能可在振动过程之后。
     *
     * @param mode 指示振动模式，可以是一次性或周期性的。详见{@link VibratorMode}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Stop)(enum VibratorMode mode);
};

/**
 * @brief 创建一个VibratorInterface实例。
 *
 * 获的马达接口实例可用于控制马达按照配置进行振动。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 2.2
 * @version 1.0
 */
const struct VibratorInterface *NewVibratorInterfaceInstance(void);

/**
 * @brief 释放VibratorInterface实例以及释放相关资源。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 2.2
 * @version 1.0
 */
int32_t FreeVibratorInterfaceInstance(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* VIBRATOR_IF_H */
/** @} */
