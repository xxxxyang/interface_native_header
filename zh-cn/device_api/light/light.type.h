/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 灯
 * @{
 *
 * @brief 灯模块对灯服务提供通用的接口能力。
 *
 * 灯模块为灯服务提供通用的接口去访问灯驱动。
 * 服务获取灯驱动对象或代理后，可以调用相关的APIs接口获取灯信息。
 * 打开或关闭灯，并根据灯id设置灯闪烁模式。
 *
 * @since 3.1
 */

/**
 * @file Light_if.h
 *
 * @brief 定义灯的数据结构，包括灯id，灯的模式，灯的闪烁模式和持续时间，灯的状态，灯的效果。
 * @since 3.1
 */

#ifndef LIGHT_TYPE_H
#define LIGHT_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 枚举灯模块的状态值。
 * @since 3.1
 */

enum LightStatus {
    /** 操作成功 */
    LIGHT_SUCCESS            = 0,
    /** 灯id不支持 */
    LIGHT_NOT_SUPPORT        = -1,
    /** 设置闪烁不支持 */
    LIGHT_NOT_FLASH          = -2,
    /** 设置亮度不支持 */
    LIGHT_NOT_BRIGHTNESS     = -3,
};

/**
 * @brief 枚举灯ids
 *
 * @since 3.1
 */
enum LightId {
    /** 未知id */
    LIGHT_ID_NONE                = 0,
    /** 电源指示灯 */
    LIGHT_ID_BATTERY             = 1,
    /** 通知灯 */
    LIGHT_ID_NOTIFICATIONS       = 2,
    /** 报警灯 */
    LIGHT_ID_ATTENTION           = 3,
    /** 无效id */
    LIGHT_ID_BUTT                = 4,
};

/**
 * @brief 枚举灯的模式
 *
 * @since 3.1
 */
enum LightFlashMode {
    /** 常亮 */
    LIGHT_FLASH_NONE     = 0,
    /** 闪烁 */
    LIGHT_FLASH_TIMED    = 1,
    /** 无效模式 */
    LIGHT_FLASH_BUTT     = 2,
};

/**
 * @brief 定义闪烁参数。
 *
 * 这些参数包括闪烁模式以及闪烁期间指示灯的打开和关闭时间。
 *
 * @since 3.1
 */
struct LightFlashEffect {
    /** 闪烁模式。详见{@link LightFlashMode}. */
    int32_t flashMode;
    /** 表示灯在闪烁期间点亮时持续的时间（毫秒）。 */
    int32_t onTime;
    /** 表示灯在闪烁期间熄灭时持续的时间（毫秒）。 */
    int32_t offTime;
};

/**
 * @brief 定义灯的效果参数。
 *
 * 参数包括亮度和闪烁模式。
 *
 * @since 3.1
 */
struct LightEffect {
    /**
     * 亮度值：Bits 24–31为扩展位，Bits 16–23为红色，bits 8–15为绿色，bits 0–7为蓝色。
     * 如果字节段不等于0，指示打开相应颜色的灯。
    */
    int32_t lightBrightness;
    /** 闪烁模式。详见{@link LightFlashEffect}. */
    struct LightFlashEffect flashEffect;
};

/**
 * @brief 定义灯的基本信息。
 *
 * 基本的灯信息包括灯id和自定义扩展信息。
 *
 * @since 3.1
 */
struct LightInfo {
    /** 灯id。 详见{@link LightId}. */
    uint32_t lightId;
    /** 自定义扩展信息。 */
    int32_t reserved;
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* LIGHT_TYPE_H */
/** @} */

