/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 灯
 * @{
 *
 * @brief 灯模块对灯服务提供通用的接口能力。
 *
 * 灯模块为灯服务提供通用的接口去访问灯驱动。
 * 服务获取灯驱动对象或代理后，可以调用相关的APIs接口获取灯信息。
 * 打开或关闭灯，并根据灯id设置灯闪烁模式。
 *
 * @since 3.1
 */

/**
 * @file Light_if.h
 *
 * @brief 定义灯的数据结构，包括灯id，灯的模式，灯的闪烁模式和持续时间，操作灯返回的状态，灯的效果。
 * @since 3.1
 */

#ifndef LIGHT_IF_H
#define LIGHT_IF_H

#include <stdint.h>
#include "light_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 定义可以在灯上执行的基本操作。
 *
 * 操作包括获取灯的信息, 打开或关闭灯，设置灯的亮度或闪烁模式。
 */

struct LightInterface {
    /**
     * @brief 获取当前系统中所有类型的灯信息。
     *
     * @param lightInfo 表示指向灯信息的二级指针。详见{@link LightInfo}。
     * @param count 表示指向灯数量的指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.1
     */
    int32_t (*GetLightInfo)(struct LightInfo **lightInfo, uint32_t *count);

    /**
     * @brief 根据指定的灯id打开列表中的可用灯。
     *
     * @param lightId 表示灯id。详见{@link LightId}。
     * @param effect 表示指向灯效果的指针，如果lightbrightness字段为0时，
     * 灯的亮度根据HCS配置的默认亮度进行设置。详见{@link LightEffect}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果不支持灯id，则返回-1。
     * @return 如果不支持闪烁设置，则返回-2。
     * @return 如果不支持亮度设置，则返回3。
     *
     * @since 3.1
     */
    int32_t (*TurnOnLight)(uint32_t lightId, struct LightEffect *effect);

    /**
     * @brief 根据指定的灯id打开列表中的可用灯。
     *
     * @param lightId 表示灯id。详见{@link LightId}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.1
     */
    int32_t (*TurnOffLight)(uint32_t lightId);
};

/**
 * @brief 创建LightInterface实例。
 *
 * 创建的LightInterface实例可用于执行相关的灯控制操作。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 3.1
 */
const struct LightInterface *NewLightInterfaceInstance(void);

/**
 * @brief 释放LightInterface实例和相关资源。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 3.1
 */
int32_t FreeLightInterfaceInstance(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* LIGHT_IF_H */
/** @} */