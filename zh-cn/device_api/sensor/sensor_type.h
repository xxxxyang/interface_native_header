/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 传感器
 * @{
 *
 * @brief 传感器设备驱动对传感器服务提供通用的接口能力。
 *
 * 模块提供传感器服务对传感器驱动访问统一接口，服务获取驱动对象或者代理后，
 * 通过其提供的各类方法，以传感器id区分访问不同类型传感器设备，实现获取传感器设备信息、订阅/去订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度，量程等可选配置等。
 *
 * @version 1.0
 */

/**
 * @file sensor_type.h
 *
 * @brief 定义传感器模块所使用的传感器类型，传感器信息，传感器数据结构等数据类型。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef SENSOR_TYPE_H
#define SENSOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**< 传感器名称的最大长度 */
#define SENSOR_NAME_MAX_LEN       32
/**< 传感器版本的最大长度 */
#define SENSOR_VERSION_MAX_LEN    16

/**
 * @brief  定义传感器模块返回值类型。
 *
 * @since 2.2
 */
enum SensorStatus {
    /**< 传感器执行成功 */
    SENSOR_SUCCESS           = 0,
    /**< 传感器执行失败 */
    SENSOR_FAILURE           = -1,
    /**< 传感器不支持 */
    SENSOR_NOT_SUPPORT       = -2,
    /**< 传感器无效参数 */
    SENSOR_INVALID_PARAM     = -3,
    /**< 传感器无效服务 */
    SENSOR_INVALID_SERVICE   = -4,
    /**< 传感器空指针 */
    SENSOR_NULL_PTR          = -5,
};

/**
 * @brief 定义传感器类型标识。
 *
 * @since 2.2
 */
enum SensorTypeTag {
    /**< 空传感器类型，用于测试 */
    SENSOR_TYPE_NONE                = 0,
    /**< 加速度传感器 */
    SENSOR_TYPE_ACCELEROMETER       = 1,
    /**< 陀螺仪传感器 */
    SENSOR_TYPE_GYROSCOPE           = 2,
    /**< 心率传感器 */
    SENSOR_TYPE_PHOTOPLETHYSMOGRAPH = 3,
    /**< 心电传感器 */
    SENSOR_TYPE_ELECTROCARDIOGRAPH  = 4,
    /**< 环境光传感器 */
    SENSOR_TYPE_AMBIENT_LIGHT       = 5,
    /**< 地磁传感器 */
    SENSOR_TYPE_MAGNETIC_FIELD      = 6,
    /**< 电容传感器 */
    SENSOR_TYPE_CAPACITIVE          = 7,
    /**< 气压计传感器 */
    SENSOR_TYPE_BAROMETER           = 8,
    /**< 温度传感器 */
    SENSOR_TYPE_TEMPERATURE         = 9,
    /**< 霍尔传感器 */
    SENSOR_TYPE_HALL                = 10,
    /**< 手势传感器 */
    SENSOR_TYPE_GESTURE             = 11,
    /**< 接近光传感器 */
    SENSOR_TYPE_PROXIMITY           = 12,
    /**< 湿度传感器 */
    SENSOR_TYPE_HUMIDITY            = 13,
    /**< 医疗传感器ID枚举值范围的开始 */
    SENSOR_TYPE_MEDICAL_BEGIN       = 128,
    /**< 医疗传感器ID枚举值范围的结束 */
    SENSOR_TYPE_MEDICAL_END         = 160,
    /**< 物理传感器最大类型 */
    SENSOR_TYPE_PHYSICAL_MAX        = 255,
    /**< 方向传感器 */
    SENSOR_TYPE_ORIENTATION         = 256,
    /**< 重力传感器 */
    SENSOR_TYPE_GRAVITY             = 257,
    /**< 线性加速度传感器 */
    SENSOR_TYPE_LINEAR_ACCELERATION = 258,
    /**< 旋转矢量传感器 */
    SENSOR_TYPE_ROTATION_VECTOR     = 259,
    /**< 环境温度传感器 */
    SENSOR_TYPE_AMBIENT_TEMPERATURE = 260,
    /**< 未校准磁场传感器 */
    SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED = 261,
    /**< 游戏旋转矢量传感器 */
    SENSOR_TYPE_GAME_ROTATION_VECTOR        = 262,
    /**< 未校准陀螺仪传感器 */
    SENSOR_TYPE_GYROSCOPE_UNCALIBRATED      = 263,
    /**< 大幅度动作传感器 */
    SENSOR_TYPE_SIGNIFICANT_MOTION          = 264,
    /**< 计步器检测传感器 */
    SENSOR_TYPE_PEDOMETER_DETECTION         = 265,
    /**< 计步器传感器 */
    SENSOR_TYPE_PEDOMETER                   = 266,
    /**< 地磁旋转矢量传感器 */
    SENSOR_TYPE_GEOMAGNETIC_ROTATION_VECTOR = 277,
    /**< 心率传感器 */
    SENSOR_TYPE_HEART_RATE                  = 278,
    /**< 设备方向传感器 */
    SENSOR_TYPE_DEVICE_ORIENTATION          = 279,
    /**< 佩戴检测传感器 */
    SENSOR_TYPE_WEAR_DETECTION              = 280,
    /**< 未校准加速度传感器 */
    SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED  = 281,
    /**< 传感器类型最大个数标r */
    SENSOR_TYPE_MAX,
};

/**
 * @brief 传感器的精度类型。
 *
 * @since 2.2
 */
enum SensorAccuracyType {
    /**< 无精度类型  */
    SENSOR_NO_ACCURACY     = 0,
     /**< 低精度类型 */
    SENSOR_LOW_ACCURACY    = 1,
    /**< 中等精度类型 */
    SENSOR_MEDIUM_ACCURACY = 2,
    /**< 高精度类型 */
    SENSOR_HIGH_ACCURACY   = 3,
    /**< 最大精度类型 */
    SENSOR_MAX_ACCURACY,
};

/**
 * @brief 传感器的量程级别。
 *
 * @since 2.2
 */
enum SensorRangeType {
    /**< 量程级别1 */
    SENSOR_RANGE_LEVEL1 = 0,
    /**< 量程级别2 */
    SENSOR_RANGE_LEVEL2 = 1,
    /**< 量程级别3 */
    SENSOR_RANGE_LEVEL3 = 2,
    /**< 量程最大级别 */
    SENSOR_RANGE_LEVEL_MAX,
};

/**
 * @brief 传感器的工作模式。
 *
 * @since 2.2
 */
enum SensorModeType {
    /**< 传感器默认工作模式状态 */
    SENSOR_MODE_DEFAULT   = 0,
    /**< 传感器实时工作模式状态，一组数据上报一次 */
    SENSOR_MODE_REALTIME  = 1,
    /**< 传感器实时工作模式状态，状态变更上报一次 */
    SENSOR_MODE_ON_CHANGE = 2,
    /**< 传感器实时工作模式状态，只上报一次 */
    SENSOR_MODE_ONE_SHOT  = 3,
    /**< 传感器缓存工作模式状态，根据配置的缓存大小上报 */
    SENSOR_MODE_FIFO_MODE = 4,
    /**< 传感器最大类型标识 */
    SENSOR_MODE_MAX,
};

/**
 * @brief 枚举传感器的硬件服务组。
 *
 * @since 2.2
 */
enum SensorGroupType {
    /** 传统传感器类型，传感器ID枚举值范围为128-160 */
    TRADITIONAL_SENSOR_TYPE = 0,
    /** 医疗传感器类型，传感器ID枚举值范围不在128-160之间 */
    MEDICAL_SENSOR_TYPE = 1,
    /** 最大传感器类型 */
    SENSOR_GROUP_TYPE_MAX,
};

/**
 * @brief 定义传感器基本信息。
 *
 * 一个传感器设备信息包括传感器名字、设备厂商、固件版本号、硬件版本号、传感器类型编号、传感器标识、最大量程、精度、功耗。
 *
 * @since 2.2
 */
struct SensorInformation {
    /**< 传感器名称 */
    char sensorName[SENSOR_NAME_MAX_LEN];
    /**< 传感器设备厂商 */
    char vendorName[SENSOR_NAME_MAX_LEN];
    /**< 传感器固件版本号 */
    char firmwareVersion[SENSOR_VERSION_MAX_LEN];
    /**< 传感器硬件版本号 */
    char hardwareVersion[SENSOR_VERSION_MAX_LEN];
    /**< 传感器类型编号，唯一标识一个传感器设备类型，详见{@link SensorTypeTag} */
    int32_t sensorTypeId;
    /**< 传感器的标识号，有传感器驱动开发者定义 */
    int32_t sensorId;
    /**< 传感器的最大量程 */
    float maxRange;
    /**< 传感器的精度 */
    float accuracy;
    /**< 传感器的功耗 */
    float power;
};

/**
 * @brief 上报传感器数据结构。。
 *
 * 上报传感器设备数据事件信息包括传感器的标识号、传感器算法版本号、传感器数据生成时间、传感器量程精度可选配置、传感器工作模式、传感器数据地址、传感器数据长度。
 *
 * @since 2.2
 */
struct SensorEvents {
    /**< 传感器的标识号 */
    int32_t sensorId;
    /**< 传感器算法版本号 */
    int32_t version;
    /**< 传感器数据生成时间 */
    int64_t timestamp;
    /**< 传感器量程精度可选配置 */
    uint32_t option;
    /**< 传感器工作模式 */
    int32_t mode;
    /**< 传感器数据地址 */
    uint8_t *data;
    /**< 传感器数据长度 */
    uint32_t dataLen;
};

/**
 * @brief 传感器上报数据回调函数的定义，传感器服务用户在订阅传感器时，
 * 需要注册上报数据回调函数，传感器使能后，传感器服务用户可以接受到传感器数据，{@link SensorInterface}。
 *
 * @since 2.2
 */
typedef int32_t (*RecordDataCallback)(const struct SensorEvents*);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* SENSOR_TYPE_H */
/** @} */