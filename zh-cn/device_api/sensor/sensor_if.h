/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 传感器
 * @{
 *
 * @brief 传感器设备驱动对传感器服务提供通用的接口能力。
 *
 * 模块提供传感器服务对传感器驱动访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现获取传感器设备信息、订阅/去订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度，量程等可选配置等。
 *
 * @since 2.2
 */

/**
 * @file sensor_if.h
 *
 * @brief  Sensor模块对外通用的接口声明文件，提供获取传感器设备信息、订阅/去订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度，量程等可选配置接口定义。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef SENSOR_IF_H
#define SENSOR_IF_H

#include "sensor_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 提供sensor设备基本控制操作接口。
 *
 * 结构体提供获取传感器设备信息、订阅/去订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度，量程等可选配置接口定义。
 */
struct SensorInterface {
    /**
     * @brief 获取当前系统中所有类型的传感器信息。
     *
     * @param sensorInfo 输出系统中注册的所有传感器信息，一种类型传感器信息包括传感器名字、设备厂商、
     * 固件版本号、硬件版本号、传感器类型编号、传感器标识、最大量程、精度、功耗。详见{@link SensorInformation}。
     * @param count 输出系统中注册的传感器数量。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*GetAllSensors)(struct SensorInformation **sensorInfo, int32_t *count);

    /**
     * @brief 根据传感器设备类型标识使能传感器信息列表里存在的设备，只有数据订阅者使能传感器后，才能获取订阅的传感器数据。
     *
     * @param sensorId 唯一标识一个传感器设备类型。详见{@link SensorTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Enable)(int32_t sensorId);

    /**
     * @brief  去使能一种传感器设备。
     *
     * @param sensorId 唯一标识一个传感器设备类型。详见{@link SensorTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Disable)(int32_t sensorId);

    /**
     * @brief 设置指定传感器的数据采样间隔和数据上报间隔。
     *
     * @param sensorId 唯一标识一个传感器设备类型。详见{@link SensorTypeTag}。
     * @param samplingInterval 设置指定传感器的数据采样间隔，单位纳秒。
     * @param reportInterval 表示传感器数据上报间隔，单位纳秒。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*SetBatch)(int32_t sensorId, int64_t samplingInterval, int64_t reportInterval);

    /**
     * @brief 设置指定传感器的数据上报模式，不同的工作模式，上报数据的方式不同。
     *
     * @param sensorId 唯一标识一个传感器设备类型。详见{@link SensorTypeTag}。
     * @param mode 传感器的数据上报模式。详见{@link SensorModeType}。
     *
     * @return 如果设置传感器数据报告模式成功，则返回0。
     * @return 如果设置传感器数据报告模式失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*SetMode)(int32_t sensorId, int32_t mode);

    /**
     * @brief 设置指定传感器量程，精度等可选配置。
     *
     * @param sensorId 唯一标识一个传感器设备类型。详见{@link SensorTypeTag}。
     * @param option 传感器的量程，精度等配置。
     *
     * @return 如果设置参数成功，则返回0。
     * @return 如果设置参数失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*SetOption)(int32_t sensorId, uint32_t option);

    /**
     * @brief 订阅者注册传感器数据回调函数，系统会将获取到的传感器数据上报给订阅者。
     *
     * @param groupId 传感器组ID。
     * sensorId枚举值范围为128-160，表示已订阅医疗传感器服务，只需成功订阅一次，无需重复订阅。

     * sensorId枚举值范围不在128-160之间，这意味着传统传感器已订阅，并且订阅成功一次。
     * @param cb 要注册的回调函数。详见{@link RecordDataCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Register)(int32_t groupId, RecordDataCallback cb);

    /**
     * @brief 订阅者去注册传感器数据回调函数。
     *
     * @param groupId 传感器组ID。
     * sensorId枚举值范围为128-160，表示已订阅医疗传感器服务。只需成功取消订阅一次，无需重复取消订阅。
     * sensorId枚举值范围不在128-160之间，这意味着传统传感器已订阅。并且成功取消订阅。
     * @param cb 要注册的回调函数。详见{@link RecordDataCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    int32_t (*Unregister)(int32_t groupId, RecordDataCallback cb);
};

/**
 * @brief 创建传感器接口实例。
 *
 * @param sensorId 表示传感器ID。有关详细信息，请参阅{@link SensorTypeTag}。
 * 可以使用该实例获取传感器信息，订阅或取消订阅传感器数据，打开或关闭传感器，
 * 设置传感器数据报告模式，并设置传感器参数，如精度和测量范围。
 * @param cb 表示要注册的回调函数。有关详细信息，请参阅{@link RecordDataCallback}。
 *
 * @return 如果创建实例成功，则返回非零值。
 * @return 如果创建实例失败，则返回负值。
 *
 * @since 2.2
 * @version 1.0
 */
const struct SensorInterface *NewSensorInterfaceInstance(void);

/**
 * @brief 释放传感器接口实例。
 *
 * @return 如果释放实例成功，则返回0。
 * @return 如果释放实例失败，则返回负值。
 * @since 2.2
 * @version 1.0
 */
int32_t FreeSensorInterfaceInstance(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* SENSOR_IF_H */
/** @} */
