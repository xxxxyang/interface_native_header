/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括input设备的打开和关闭、input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 * @version 1.0
 */

 /**
 * @file input_type.h
 *
 * @brief input设备相关的类型定义，定义了input设备驱动接口所使用的结构体及枚举类型。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_TYPES_H
#define INPUT_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_INPUT_DEV_NUM 32
#define MAX_NODE_PATH_LEN 64
#define CHIP_INFO_LEN 10
#define CHIP_NAME_LEN 10
#define VENDOR_NAME_LEN 10
#define SELF_TEST_RESULT_LEN 20

/**
 * @brief 定义返回值类型
 */
enum RetStatus {
    /**
    * 成功。
    */
    INPUT_SUCCESS        = 0,
    
    /**
    * 失败。
    */
    INPUT_FAILURE        = -1,

    /**
    * 无效参数。
    */
    INPUT_INVALID_PARAM  = -2,

    /**
    * 内存不足。
    */
    INPUT_NOMEM          = -3,

    /**
    * 空指针。
    */
    INPUT_NULL_PTR       = -4,

    /**
    * 执行超时。
    */
    INPUT_TIMEOUT        = -5,

    /**
    * 特性不支持。
    */
    INPUT_UNSUPPORTED    = -6,
};

/**
 * @brief 定义input设备类型
 */
enum InputDevType {
    /**
    * 触摸屏。
    */
    INDEV_TYPE_TOUCH,

    /**
    * 物理按键。
    */
    INDEV_TYPE_KEY,

    /**
    * 键盘。
    */
    INDEV_TYPE_KEYBOARD,

    /**
    * 鼠标。
    */
    INDEV_TYPE_MOUSE,

    /**
    * 虚拟按键。
    */
    INDEV_TYPE_BUTTON,

    /**
    * 表冠。
    */
    INDEV_TYPE_CROWN,

    /**
    * 自定义编码的特定功能或者事件。
    */
    INDEV_TYPE_ENCODER,

    /**
    * 未知输入设备类型。
    */
    INDEV_TYPE_UNKNOWN,
};

/**
 * @brief 定义电源状态
 */
enum PowerStatus {
    /**
    * 正常唤醒。
    */
    INPUT_RESUME,

    /**
    * 休眠下电模式。
    */
    INPUT_SUSPEND,

    /**
    * 休眠低功耗模式。
    */
    INPUT_LOW_POWER,

    /**
    * 未知电源状态。
    */
    INPUT_POWER_STATUS_UNKNOWN,
};

/**
 * @brief 定义容值测试类型
 */
enum CapacitanceTest {
    /**
    * 基础容值测试
    */
    BASE_TEST,

    /**
    * 全量容值自检测试
    */
    FULL_TEST,

    /**
    * MMI容值测试
    */
    MMI_TEST,

    /**
    * 老化容值测试
    */
    RUNNING_TEST,

    /**
    * 未知的测试类型
    */
    TEST_TYPE_UNKNOWN,
};

/**
 * @brief input事件数据包结构
 */
typedef struct {
    /**
    * 输入事件的属性
    */
    uint32_t type;

    /**
    * 输入事件的特定编码项
    */
    uint32_t code;

    /**
    * 输入事件编码项对应的值
    */
    int32_t value;

    /**
    * 输入事件对应的时间戳
    */
    uint64_t timestamp;
} EventPackage;

/**
 * @brief 此结构体定义了输入事件回调函数并提供给input服务使用
 */
typedef struct {
    /**
     * @brief 输入事件数据上报的回调函数
     *
     * @param eventData 驱动上报的input事件数据
     * @param count Input事件数据包的个数
     * @param devIndex Input设备索引，用于标志多个input设备，取值从0开始，最多支持32个设备
     *
     * @since 1.0
     * @version 1.0
     */
    void (*ReportEventPkgCallback)(const EventPackage **pkgs, uint32_t count, uint32_t devIndex);

    /**
     * @brief 热插拔事件上报的回调函数
     *
     * @param event 上报的热插拔事件数据
     *
     * @since 1.0
     * @version 1.0
     */
    void (*ReportHotPlugEventCallback)(const HotPlugEvent* event);
} InputReportEventCb;

/**
 * @brief input设备基础设备信息
 */
typedef struct {
    /**
    * 设备索引
    */
    uint32_t devIndex;

    /**
    * 设备对应的文件描述符
    */
    int32_t fd;

    /**
    * 设备对应的服务
    */
    void *service;

    /**
    * 设备对应的事件监听器
    */
    void *listener;

    /**
    * 设备类型
    */
    uint32_t devType;

    /**
    * 电源状态
    */
    uint32_t powerStatus;

    /**
    * 驱动芯片编码信息
    */
    char chipInfo[CHIP_INFO_LEN];

    /**
    * 模组厂商名
    */
    char vendorName[VENDOR_NAME_LEN];

    /**
    * 驱动芯片型号
    */
    char chipName[CHIP_NAME_LEN];

    /**
    * 设备文件路径
    */
    char devNodePath[MAX_NODE_PATH_LEN];

    /**
    * x方向分辨率
    */
    uint32_t solutionX;

    /**
    * y方向分辨率
    */
    uint32_t solutionY;

    /**
    * 数据上报回调函数 {@link InputReportEventCb}
    */
    InputReportEventCb *callback;
} DeviceInfo;

/**
 * @brief 扩展指令的数据结构
 */
typedef struct {
    /**
    * 指令对应的编码
    */
    const char *cmdCode;

    /**
    * 指令传输的数据
    */
    const char *cmdValue;
} InputExtraCmd;

#ifdef __cplusplus
}
#endif
#endif
/** @} */
