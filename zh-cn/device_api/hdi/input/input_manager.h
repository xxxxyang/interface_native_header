/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括input设备的打开和关闭、input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file input_manager.h
 *
 * @brief 描述input设备管理相关的接口声明。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "input_controller.h"
#include "input_reporter.h"
#include "input_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供input设备管理相关的接口。
 *
 * 此类接口包含input设备的扫描、打开和关闭、特定设备信息查询，以及所有设备列表信息获取等接口。
 */
typedef struct {
    /**
     * @brief input服务用于扫描所有在线设备。
     *
     * @param staArr 存放Input设备扫描信息的数组，信息包含设备索引以及设备类型。
     * @param arrLen staArr数组的长度信息。
     *
     * @return Returns INPUT_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     *
     * @since 1.0
     */
    int32_t (*ScanInputDevice)(DevDesc *staArr, uint32_t arrLen);

    /**
     * @brief input服务打开对应设备的设备文件
     *
     * @param devIndex Input设备索引，用于标志多个input设备，取值从0开始，最多支持32个设备。
     *
     * @return Returns INPUT_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*OpenInputDevice)(uint32_t devIndex);

    /**
     * @brief input服务关闭对应设备的设备文件
     *
     * @param devIndex Input设备索引，用于标志多个input设备，取值从0开始，最多支持32个设备
     *
     * @return Returns INPUT_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*CloseInputDevice)(uint32_t devIndex);

    /**
     * @brief input服务获取对应ID的设备信息
     *
     * @param devIndex Input设备索引，用于标志多个input设备，取值从0开始，最多支持32个设备
     * @param devInfo 即devIndex对应的设备的设备信息{具体参考 @link DeviceInfo}
     *
     * @return Returns INPUT_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetInputDevice)(uint32_t devIndex, DeviceInfo **devInfo);

    /**
     * @brief input服务获取所有input设备列表的设备信息
     *
     * @param devNum 当前已经注册过的所有input设备的总数
     * @param devInfo input设备列表所对应的设备信息{具体参考 @link DeviceInfo}
     * @param size 即指定deviceList数组对应的元素个数
     *
     * @return Returns INPUT_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetInputDeviceList)(uint32_t *devNum, DeviceInfo **devList, uint32_t size);
} InputManager;

/**
 * @brief Defines interfaces for providing driver capabilities of input devices.
 */
typedef struct {
    InputManager *iInputManager;          /**< input设备的设备管理接口 */
    InputController *iInputController;    /**< input设备的业务控制接口 */
    InputReporter *iInputReporter;        /**< input设备的数据上报接口 */
} IInputInterface;

/**
 * @brief input服务通过调用此接口获取操作input设备的所有接口。
 *
 * @param interface 对input设备进行接口操作的指针，通常在input服务启动后，通过调用此函数获取input设备操作接口
 *
 * @return Returns INPUT_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GetInputInterface(IInputInterface **interface);

/**
 * @brief input服务通过调用此接口释放操作input设备的所有接口。
 *
 * @param inputInterface 对input设备进行接口操作的指针
 *
 * @return Returns INPUT_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link RetSatus}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t ReleaseInputInterface(IInputInterface *inputInterface)

#ifdef __cplusplus
}
#endif
#endif
/** @} */