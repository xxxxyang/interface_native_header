/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file icamera_host.h
 *
 * @brief Camera服务的管理类，对上层提供HDI接口。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_CAMERA_HOST_CLIENT_INF_H
#define HDI_CAMERA_HOST_CLIENT_INF_H

#include <list>
#include <map>
#include <vector>
#include "icamera_device_callback.h"
#include "types.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class ICameraDevice;
class ICameraHostCallback;
class ICameraDeviceCallback;
class ICameraHost : public ICameraInterface {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.Host");
    virtual ~ICameraHost() {}
    /**
     * @brief 获取ICameraHost实例。
     *
     * 此接口为camera调用入口，需要先通过该接口获取ICameraHost实例，然后通过ICameraHost实例进行其它操作。
     *
     * @param serviceName [IN] 要获取的ICameraHost实例的名称，当前实现中实例名称固定为camera_service。
     *
     * @return Returns 成功返回ICameraHost实例，失败返回nullptr。
     *
     * @since 1.0
     * @version 1.0
     */
    static sptr<ICameraHost> Get(const char *serviceName);

    /**
     * @brief 设置ICameraHost回调接口，回调函数参考 {@link ICameraHostCallback}。
     *
     * @param callback [IN] 要设置的回调函数。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode SetCallback(const OHOS::sptr<ICameraHostCallback> &callback) = 0;

    /**
     * @brief 获取当前可用的Camera设备ID列表。
     *
     * @param cameraIds [OUT] 返回当前可用的设备列表
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see GetCameraAbility
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode GetCameraIds(std::vector<std::string> &cameraIds) = 0;

    /**
     * @brief 获取Camera设备能力集合。
     *
     * @param cameraId [IN] 用于指定要操作的camera设备，通过 {@link GetCameraIds} 获取。
     *
     * @param ability [OUT] 返回cameraId对应Camera设备的能力集合。
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode GetCameraAbility(const std::string &cameraId,
        std::shared_ptr<CameraAbility> &ability) = 0;

    /**
     * @brief 打开Camera设备。
     *
     * 打开指定的Camera设备，通过此接口可以获取到ICameraDevice对象，通过ICameraDevice对象可以操作具体的Camera设备。
     *
     * @param cameraId [IN] 需要打开的Camera设备ID，可通过 {@link GetCameraIds} 接口获取当前已有Camera设备列表。
     * @param callback [IN] camera设备相关的回调函数，具体参见 {@link ICameraDeviceCallback}。
     * @param device [OUT] 返回当前要打开的Camera设备ID对应的ICameraDevice对象。
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode OpenCamera(const std::string &cameraId,
        const OHOS::sptr<ICameraDeviceCallback> &callback,
        OHOS::sptr<ICameraDevice> &device) = 0;

    /**
     * @brief 打开或关闭闪光灯。
     *
     * 该接口只能由打开cameraId指定Camera设备的调用者调用。
     *
     * @param cameraId [IN] 闪光灯对应的camera设备ID。
     * @param isEnable [IN] true打开闪光灯，false关闭闪光灯。
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode SetFlashlight(const std::string &cameraId, bool &isEnable) = 0;
};
}
#endif /** HDI_CAMERA_HOST_CLIENT_INF_H