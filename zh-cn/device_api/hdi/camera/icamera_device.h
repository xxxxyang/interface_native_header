/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file icamera_device.h
 *
 * @brief Camera设备操作接口。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_CAMERA_DEVICE_CLIENT_INF_H
#define HDI_CAMERA_DEVICE_CLIENT_INF_H

#include <list>
#include <map>
#include <vector>
#include "types.h"
#include "icamera_device_callback.h"
#include "istream_operator.h"
#include "istream_operator_callback.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class ICameraDevice : public ICameraInterface {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.Device");
    virtual ~ICameraDevice() {}

    /**
     * @brief 获取流操作句柄。
     *
     * @param callback [IN] 设置流回调接口，详细可查看 {@link IStreamOperatorCallback}，
     * 用于上报捕获开始 {@link OnCaptureStarted}，捕获结束 {@link OnCaptureEnded}，
     * {@link OnCaptureError} 捕获错误等信息。
     *
     * @param streamOperator [OUT] 返回流操作句柄。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode GetStreamOperator(
        const OHOS::sptr<IStreamOperatorCallback> &callback,
        OHOS::sptr<IStreamOperator> &streamOperator) = 0;

    /**
     * @brief 更新设备控制参数。
     *
     * @param settings [IN] Camera设置参数，包括sensor帧率，3A相关参数等。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode UpdateSettings(const std::shared_ptr<CameraSetting> &settings) = 0;

    /**
     * @brief 设置metadata上报模式，逐帧上报还是设备状态变化时上报。
     *
     * @param mode [IN] metadata的上报模式，逐帧上报或者设备状态变化时上报，查看 {@link ResultCallbackMode}。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode SetResultMode(const ResultCallbackMode &mode) = 0;

    /**
     * @brief 查询使能的metadata。
     *
     * {@link EnableResult} 使能需要上报的metadata之后，可通过此接口查询使能的metadata。
     *
     * @param results [OUT] 所有使能的metadata。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode GetEnabledResults(std::vector<MetaType> &results) = 0;

    /**
     * @brief 打开metadata上报开关。
     *
     * {@link OnResult} 只上报此接口使能后的metadata。
     *
     * @param results [IN] 需要打开上报开关的metadata。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see DisableResult
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode EnableResult(const std::vector<MetaType> &results) = 0;

    /**
     * @brief 关闭metadata上报开关。
     *
     * 屏蔽之后，相应的{@link OnResult}不再上报，需 {@link EnableResult} 使能之后才上报。
     *
     * @param results [IN] 需要关闭上报开关的metadata。
     *
     * @return Returns NO_ERROR 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @see EnableResult
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode DisableResult(const std::vector<MetaType> &results) = 0;

    /**
     * @brief 关闭Camera设备。
     *
     * @see OpenCamera
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void Close() = 0;
};
}
#endif /** HDI_CAMERA_DEVICE_CLIENT_INF_H