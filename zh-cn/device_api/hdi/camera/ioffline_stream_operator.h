/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file ioffline_stream_operator.h
 *
 * @brief 离线流的操作接口。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_OFFLINE_STREAM_OPERATOR_CLIENT_INF_H
#define HDI_OFFLINE_STREAM_OPERATOR_CLIENT_INF_H

#include <list>
#include <map>
#include <vector>
#include "types.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class IOfflineStreamOperator : public ICameraInterface {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.OfflineStreamOperator");

    virtual ~IOfflineStreamOperator() {}

    /**
     * @brief 取消捕获请求。
     *
     * @param captureId [IN] 用于标识要取消的捕获请求。
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode CancelCapture(int captureId) = 0;

    /**
     * @brief 释放离线流。
     *
     * @param streamIds 用于标识要释放的离线流集合。
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode ReleaseStreams(const std::vector<int> &streamIds) = 0;

    /**
     * @brief 释放所有离线流。
     *
     *
     * @return Returns NO_ERROR 表示执行成功；
     * @return Returns 其他值表示执行失败，具体错误码查看 {@link CamRetCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual CamRetCode Release() = 0;
};
}
#endif /** HDI_OFFLINE_STREAM_OPERATOR_CLIENT_INF_H