/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file istream_operator_callback.h
 *
 * @brief {@link IStreamOperator} 相关的回调，这些回调均由调用者实现。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_STREAM_OPERATOR_CALLBACK_SERVER_H
#define HDI_STREAM_OPERATOR_CALLBACK_SERVER_H

#include <list>
#include <map>
#include <vector>
#include "types.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class IStreamOperatorCallback : public ICameraInterface {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.StreamOperatorCallback");
    virtual ~IStreamOperatorCallback() {}

public:
    /**
     * @brief 捕获开始回调，在捕获开始时调用。
     *
     * @param captureId [IN] 用于标识回调对应的捕获请求。
     * @param streamIds [IN] 回调对应的流集合。
     *
     * @see OnCaptureEnded
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnCaptureStarted(int32_t captureId, const std::vector<int32_t> &streamIds) = 0;

    /**
     * @brief 捕获结束回调，在捕获结束时调用。
     *
     * @param captureId [IN] 用于标识回调对应的捕获请求。
     * @param infos [IN] 捕获结束相关信息。
     *
     * @see OnCaptureStarted
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnCaptureEnded(int32_t captureId,
        const std::vector<std::shared_ptr<CaptureEndedInfo>> &infos) = 0;

    /**
     * @brief 捕获错误回调，在捕获过程中发生错误时调用。
     *
     * @param captureId [IN] 用于标识回调对应的捕获请求。
     * @param infos [IN] 捕获错误信息列表。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnCaptureError(int32_t captureId,
        const std::vector<std::shared_ptr<CaptureErrorInfo>> &infos) = 0;

    /**
     * @brief 帧捕获回调。
     *
     * 通过 {@link Capture} 的输入参数 {@link CaptureInfo} 的enableShutterCallback_使能该回调，
     * 使能后每次捕获均会触发此回调。
     *
     * @param captureId [IN] 用于标识回调对应的捕获请求。
     * @param streamIds [IN] 回调对应的流集合。
     * @param timestamp [IN] 该接口被调用时的时间戳。
     *
     * @see Capture
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnFrameShutter(int32_t captureId,
        const std::vector<int32_t> &streamIds, uint64_t timestamp) = 0;
};
}
#endif /** HDI_STREAM_OPERATOR_CALLBACK_SERVER_H