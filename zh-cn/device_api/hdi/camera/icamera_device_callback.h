/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file icamera_device_callback.h
 *
 * @brief Camera设备的回调接口，主要包含camera设备发生错误时和上报metadata的回调函数。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_CAMERA_DEVICE_CALLBACK_SERVER_H
#define HDI_CAMERA_DEVICE_CALLBACK_SERVER_H

#include <list>
#include <map>
#include <vector>
#include "types.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class ICameraDeviceCallback : public ICameraInterface {
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.DeviceCallback");
    virtual ~ICameraDeviceCallback() {}

public:
    /**
     * @brief 设备发生错误时调用，由调用者实现，用于返回错误信息给调用者。
     *
     * @param type [IN] 错误类型，具体错误类型可参考 {@link ErrorType}。
     * @param errorCode [IN] 错误码，当前暂未使用。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnError(ErrorType type, int32_t errorCode) = 0;

    /**
     * @brief 上报camera设备相关的metadata的回调，上报方式查看 {@link SetResultMode}。
     *
     * @param timestamp [IN] metadata上报的时间戳。
     * @param result [IN] 上报的metadata，上报的metadata由 {@link EnableResult} 指定，
     * 可通过 {@link GetEnabledResults} 查询，{@link DisableResult} 关闭上报开关。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnResult(uint64_t timestamp, const std::shared_ptr<CameraMetadata> &result) = 0;
};
}
#endif /** HDI_CAMERA_DEVICE_CALLBACK_SERVER_H