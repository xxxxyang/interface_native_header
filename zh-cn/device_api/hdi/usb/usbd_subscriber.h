/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 定义（USB）功能的标准API接口。
 *
 * 该模块用于获取描述符、接口对象、请求对象和提交请求的自定义数据类型和函数。
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usbd_subscriber.h
 *
 * @brief usb驱动订阅模块。
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USBD_SUBSCRIBER_H
#define USBD_SUBSCRIBER_H

#include "ipc_object_stub.h"
#include "usb_info.h"
#include "usbd_type.h"

namespace OHOS {
namespace USB {
class UsbdSubscriber : public IPCObjectStub {
public:
    explicit UsbdSubscriber() : IPCObjectStub(u"ohos.usb.IUsbManagerSubscriber") {};
    virtual ~UsbdSubscriber() = default;
        
    /**
     * @brief 设备事件
     *
     * @param UsbInfo 输入参数，usb设备信息
     *
     * @return 0 表示操作成功
     * @return 非零值 表示操作失败
     *
     * @since 3.0
     * @version 1.0
     */
    virtual int32_t DeviceEvent(const UsbInfo &info) = 0;
    
    /**
     * @brief 端口改变事件
     *
     * @param portId 输入参数，端口ID
     * @param powerRole 输入参数，电源角色的值
     * @param dataRole 输入参数，数据角色的值
     * @param mode 输入参数，端口模式的值
     *
     * @return 0 表示操作成功
     * @return 非零值 表示操作失败
     *
     * @since 3.0
     * @version 1.0
     */
    virtual int32_t PortChangedEvent(int32_t portId, int32_t powerRole, int32_t dataRole, int32_t mode) = 0;
    
    /**
     * @brief 远程请求
     *
     * @param code 输入参数，命令字
     * @param data 输入参数，待解析的数据
     * @param reply 输出参数，返回的数据
     * @param option 输入参数，选项数据
     *
     * @return 0 表示操作成功
     * @return 非零值 表示操作失败
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;

private:
    /**
     * @brief 解析usb设备信息
     *
     * @param code 输入参数，命令字
     * @param reply 输出参数，返回的数据
     * @param option 输入参数，选项数据
     * @param info 输出参数，usb设备信息
     *
     * @return 0 表示操作成功
     * @return 非零值 表示操作失败
     *
     * @since 3.0
     * @version 1.0
     */
    static int32_t ParserUsbInfo(MessageParcel &data, MessageParcel &reply, MessageOption &option, UsbInfo &info);
    
    /**
     * @brief 解析usb设备端口信息
     *
     * @param code 输入参数，命令字
     * @param reply 输出参数，返回的数据
     * @param option 输入参数，选项数据
     * @param info 输出参数，usb设备端口信息
     *
     * @return 0 表示操作成功
     * @return 非零值 表示操作失败
     *
     * @since 3.0
     * @version 1.0
     */
    static int32_t ParserPortInfo(MessageParcel &data, MessageParcel &reply, MessageOption &option, PortInfo &info);
};
} /** namespace USB */
} /** namespace OHOS */
#endif /** USBD_SUBSCRIBER_H */