/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 定义（USB）功能的标准API接口。
 *
 * 该模块用于获取描述符、接口对象、请求对象和提交请求的自定义数据类型和函数。
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usbd_type.h
 *
 * @brief usb驱动模块接口定义中使用的自定义数据类型。
 *
 * usb驱动模块接口定义中使用的自定义数据类型, 包括分发的命令字、数据方向等。
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USBD_TYPE_H
#define USBD_TYPE_H

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <vector>

/** usb设备最大接口数量 */
#define USB_MAX_INTERFACES 32

/** 从地址中提取USB Endpoint方向的位掩码 */
static const int32_t USB_ENDPOINT_DIR_MASK = 0x80;

/** USB Endpoint从设备到主机的数据方向 */
static const int32_t USB_ENDPOINT_DIR_IN = 0x80;

/** USB Endpoint从主机到设备的数据方向 */
static const int32_t USB_ENDPOINT_DIR_OUT = 0;

/** 请求回调函数 */
typedef void (*UsbdRequestCallback)(uint8_t *requestArg);

/**
 * @brief 批量回调命令字
 *
 */
enum UsbdBulkCbCmd {
    /** 批量回调读取 */
    CMD_USBD_BULK_CALLBACK_READ,
    /** 批量回调写入 */
    CMD_USBD_BULK_CALLBACK_WRITE,
};

/**
 * @brief 主机端和设备端插拔事件
 *
 */
enum UsbdDeviceAction {
    /** 主机端接入设备 */
    ACT_DEVUP = 0,
    /** 主机端拔出设备 */
    ACT_DEVDOWN,
    /** 设备连接 */
    ACT_UPDEVICE,
    /** 设备断开 */
    ACT_DOWNDEVICE,
};

namespace OHOS {
namespace USB {
/**
 * @brief usb设备
 *
 */
struct UsbDev {
    /** usb总线编号 */
    uint8_t busNum;
    /** usb设备地址 */
    uint8_t devAddr;
};

/**
 * @brief 管道信息
 *
 */
struct UsbPipe {
    /** usb设备接口ID */
    uint8_t interfaceId;
    /** usb设备端点ID */
    uint8_t endpointId;
};

/**
 * @brief usb控制传输
 *
 */
struct UsbCtrlTransfer {
    /** 请求类型 */
    int32_t requestType;
    /** 请求命令字 */
    int32_t requestCmd;
    /** 请求值 */
    int32_t value;
    /** 索引 */
    int32_t index;
    /** 超时时间 */
    int32_t timeout;
};

/**
 * @brief usb设备信息
 *
 */
struct USBDeviceInfo {
    /** usb设备状态 */
    int32_t status;
    /** usb总线编号 */
    int32_t busNum;
    /** usb设备编号 */
    int32_t devNum;
};
} /** namespace USB */
} /** namespace OHOS */

#endif /** USBD_TYPE_H */
