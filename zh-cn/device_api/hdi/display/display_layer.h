/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file display_layer.h
 *
 * @brief 显示图层驱动接口声明。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef DISPLAY_LAYTER_H
#define DISPLAY_LAYTER_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 显示图层驱动接口结构体，定义显示图层驱动接口函数指针。
 */
typedef struct {
    /**
     * @brief 初始化显示设备。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see DeinitDisplay
     * @since 1.0
     * @version 1.0
     */
    int32_t (*InitDisplay)(uint32_t devId);

    /**
     * @brief 取消初始化显示设备。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see InitDisplay
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DeinitDisplay)(uint32_t devId);

    /**
     * @brief 获取显示设备相关信息。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param dispInfo 显示相关信息。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayInfo)(uint32_t devId, DisplayInfo *dispInfo);

    /**
     * @brief 打开图层
     *
     * GUI在使用图层时，需要先根据图层信息打开图层，打开图层成功可获得图层ID，根据图层ID使用图层各接口。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerInfo 图层信息，上层GUI打开图层时需传递图层信息，包括图层类型，图层大小，像素格式等信息。
     * @param layerId 图层ID，打开图层成功后返回给GUI的图层ID，用于标识唯一的图层。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see CloseLayer
     * @since 1.0
     * @version 1.0
     */
    int32_t (*OpenLayer)(uint32_t devId, const LayerInfo *layerInfo, uint32_t *layerId);

    /**
     * @brief 关闭图层
     *
     * 在完成显示后，如果不在需要使用图层，调用关闭图层接口关闭图层。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see OpenLayer
     * @since 1.0
     * @version 1.0
     */
    int32_t (*CloseLayer)(uint32_t devId, uint32_t layerId);

    /**
     * @brief 设置图层是否可见
     *
     * 不可见情况下图层不显示在屏幕上，可见情况下图层显示在屏幕上。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param visible 待设置的图层可见标识，设置true表示图层可见，设置false表示图层不可见。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerVisibleState
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerVisible)(uint32_t devId, uint32_t layerId, bool visible);

    /**
     * @brief  获取图层是否可见状态。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param visible 保存获取的图层可见状态，为true表示图层可见，为false表示图层不可见。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerVisible
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerVisibleState)(uint32_t devId, uint32_t layerId, bool *visible);

    /**
     * @brief 设置图层大小。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param rect 待设置的图层大小，单位为像素。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerSize
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerSize)(uint32_t devId, uint32_t layerId, IRect *rect);

    /**
     * @brief 获取图层大小。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param rect 保存获取的图层大小。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerSize
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerSize)(uint32_t devId, uint32_t layerId, IRect *rect);

    /**
     * @brief 设置图层裁剪区域。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param rect 待设置的裁剪区域。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerCrop)(uint32_t devId, uint32_t layerId, IRect *rect);

    /**
     * @brief 设置图层Z轴次序
     *
     * 图层的Z序值越大，图层越靠上显示。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param zorder 待设置的图层Z序，为整数值，取值范围为[0, 255]，值越大图层越往上排列。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerZorder
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerZorder)(uint32_t devId, uint32_t layerId, uint32_t zorder);

    /**
     * @brief 获取图层Z轴次序。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param zorder 保存获取的图层Z轴次序，为整数值，取值范围为[0, 255]，值越大图层越往上排列。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerZorder
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerZorder)(uint32_t devId, uint32_t layerId, uint32_t *zorder);

    /**
     * @brief 设置图层预乘。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param preMul 待设置的图层预乘使能标识，1表示使能图层预乘，0表示不使能图层预乘。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerPreMulti
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerPreMulti)(uint32_t devId, uint32_t layerId, bool preMul);

    /**
     * @brief 获取图层预乘标识。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param preMul 保存获取的图层预乘使能标识。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerPreMulti
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerPreMulti)(uint32_t devId, uint32_t layerId, bool *preMul);

    /**
     * @brief 设置图层alpha值。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param alpha 待设置的图层alpha值。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerAlpha
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerAlpha)(uint32_t devId, uint32_t layerId, LayerAlpha *alpha);

    /**
     * @brief 获取图层alpha值。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param alpha 保存获取的图层alpha值。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerAlpha
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerAlpha)(uint32_t devId, uint32_t layerId, LayerAlpha *alpha);

    /**
     * @brief 设置图层colorkey属性,在图层叠加时使用。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param enable 待设置的色键使能标识。
     * @param key 待设置的色键值，即颜色值。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerColorKey
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerColorKey)(uint32_t devId, uint32_t layerId, bool enable, uint32_t key);

    /**
     * @brief 获取图层colorkey。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param enable 保存获取的enable 色键使能标识。
     * @param key 保存获取的色键值，即颜色值。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerColorKey
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerColorKey)(uint32_t devId, uint32_t layerId, bool *enable, uint32_t *key);

    /**
     * @brief 设置图层调色板。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param palette 待设置的图层调色板。
     * @param len 调色板长度。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerPalette
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerPalette)(uint32_t devId, uint32_t layerId, uint32_t *palette, uint32_t len);

    /**
     * @brief 获取图层调色板。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param palette 保存获取的图层调色板。
     * @param len 调色板长度。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerPalette
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerPalette)(uint32_t devId, uint32_t layerId, uint32_t *palette, uint32_t len);

    /**
     * @brief 设置图层变换模式，根据不同的场景设置图层的旋转、缩放、移位等。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param type 待设置的图层变换模式。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetTransformMode)(uint32_t devId, uint32_t layerId, TransformType type);

    /**
     * @brief 设置图层压缩功能
     *
     * 在特定场景下，需要对图像数据进行压缩，可设置启动或关闭图层压缩功能。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param compType 图层压缩使能标识。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see GetLayerCompression
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerCompression)(uint32_t devId, uint32_t layerId, int32_t compType);

    /**
     * @brief 获取图层压缩功能是否打开。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param compType 保存获取的图层压缩功能状态。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see SetLayerCompression
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerCompression)(uint32_t devId, uint32_t layerId, int32_t *compType);

    /**
     * @brief 设置图层刷新区域
     *
     * GUI图形系统绘制好图像数据后，在调用Flush接口刷新屏幕之前需要设置图层刷新区域。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param region 待设置的刷新区域。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetLayerDirtyRegion)(uint32_t devId, uint32_t layerId, IRect *region);

    /**
     * @brief 获取图层的buffer
     *
     * 向buffer中绘图后，调用Flush接口显示到屏幕上。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param buffer 保存获取的图层buffer。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see Flush
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetLayerBuffer)(uint32_t devId, uint32_t layerId, LayerBuffer *buffer);

    /**
     * @brief 刷新图层
     *
     * 将buffer显示数据刷新到指定的layerId图层上，实现图像数据显示到屏幕上。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param buffer 待刷新的buffer。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*Flush)(uint32_t devId, uint32_t layerId, LayerBuffer *buffer);

    /**
     * @brief 实现等待帧消隐期到来功能
     *
     * 该函数会让系统等待，直到帧消隐期到来，用于软件和硬件之间的同步。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param layerId 图层ID，图层的唯一标识，根据图层ID操作图层。
     * @param timeOut 超时时间，在设置的超时时间后，没有等到帧消隐期到来则超时返回。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*WaitForVBlank)(uint32_t devId, uint32_t layerId, int32_t timeOut);

    /**
     * @brief 实现抓图功能
     *
     * 本函数将显示设备上的图像数据截图保存到buffer中，用于调试、应用截图等场景。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param buffer 保存截屏的buffer信息。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * otherwise.
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SnapShot)(uint32_t devId, LayerBuffer *buffer);
} LayerFuncs;

/**
 * @brief 实现图层初始化功能，申请图层使用的资源，并获取图层提供的操作接口。
 *
 * @param funcs 图层操作接口指针，初始化图层时分配内存，调用者不需要分配内存，调用者获取该指针操作图层。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @see LayerUninitialize
 * @since 1.0
 * @version 1.0
 */
int32_t LayerInitialize(LayerFuncs **funcs);

/**
 * @brief 取消图层初始化功能，释放图层使用到的资源，并释放图层操作接口指针
 *
 * @param funcs 图层操作接口指针，用于释放图层初始化函数中分配的操作指针内存。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @see LayerInitialize
 * @since 1.0
 * @version 1.0
 */
int32_t LayerUninitialize(LayerFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif
/** @} */