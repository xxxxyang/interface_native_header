/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* *
 * @addtogroup Display
 * @{
 *
 * @brief 定义显示硬件驱动模块。
 *
 * 该驱动函数提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 3.0
 */

/* *
 * @file display_vgu.h
 *
 * @brief 该文件定义2D矢量硬件加速模块相关驱动函数。
 *
 * @since 3.0
 */

#ifndef DISPLAY_VGU_H
#define DISPLAY_VGU_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

#undef HDI_VGU_SCALAR_IS_FLOAT
#define HDI_VGU_SCALAR_IS_FLOAT 1

#ifdef HDI_VGU_SCALAR_IS_FLOAT
typedef float VGUScalar;
#else
typedef int32_t VGUScalar;
#endif

/**
* 像素格式
*/
typedef PixelFormat VGUPixelFormat;

/**
* 混合操作类型
*/
typedef BlendType VGUBlendType;

/* *
 * @brief 路径坐标数据类型。
 *
 */
typedef enum {
    /**
    * 整型(2 bytes)
    */
    VGU_DATA_TYPE_S16 = 0,

    /**
    * 整型(4 bytes)
    */
    VGU_DATA_TYPE_S32,

    /**
    * 浮点(4 bytes)
    */
    VGU_DATA_TYPE_F32
} VGUPathDataType;

/**
 * @brief 硬件加速能力。
 *
 */
typedef enum {
    /**
    * 支持Blit能力
    */
    VGU_CAP_BLIT = (1 << 0),

    /**
    * 搬移支持图片叠加最大数目
    */
    VGU_CAP_BLIT_NUM = (1 << 1),

    /**
    * 支持路径的fill和stroke能力
    */
    VGU_CAP_PATH = (1 << 2),

    /**
    * 支持模糊能力
    */
    VGU_CAP_FILTER_BLUR = (1 << 3),
} VGUCapability;

/**
 * @brief 错误码定义。
 *
 */
typedef enum {
    /**
    * 成功
    */
    VGU_SUCCESS = 0,

    /**
    * 不支持
    */
    VGU_NO_SUPPORT = -1,

    /**
    * 操作失败
    */
    VGU_OPERATION_FAILED = -2,

    /**
    * 内存溢出
    */
    VGU_OUT_OF_MEMORY = -3,

    /**
    * 超时
    */
    VGU_TIMEOUT = -4,

    /**
    * 无效参数
    */
    VGU_INVALID_PARAMETER = -5,

    /**
    * 设备忙
    */
    VGU_BUSY = -6,

    /**
    * 无上下文
    */
    VGU_NO_CONTEXT = -7,
} VGUResult;

/* *
 * @brief 线帽。
 *
 */
typedef enum {
    /**
    * 线条末端为平直线帽(默认)
    */
    VGU_LINECAP_BUTT = 0,

    /**
    * 线条末端为圆形线帽
    */
    VGU_LINECAP_ROUND,

    /**
    * 线条末端为正方向形线帽
    */
    VGU_LINECAP_SQUARE
} VGULineCap;

/**
 * @brief 联接类型。
 *
 */
typedef enum {
    /**
    * 尖角(默认)
    */
    VGU_LINE_JOIN_MITER = 0,

    /**
    * 圆角
    */
    VGU_LINE_JOIN_ROUND,

    /**
    * 斜角
    */
    VGU_LINE_JOIN_BEVEL,

    /**
    * 无效定义
    */
    VGU_LINE_JOIN_BUTT
} VGUJointType;

/**
 * @brief 坐标点对象。
 *
 */
typedef struct {
    /**
    * 点水平坐标
    */
    VGUScalar x;

    /**
    * 点垂直坐标
    */
    VGUScalar y;
} VGUPoint;

/**
 * @brief 矩形对象。
 *
 */
typedef struct {
    /**
    * 矩形水平起始坐标
    */
    VGUScalar x;

    /**
    * 矩形垂直起始坐标
    */
    VGUScalar y;

    /**
    * 矩形宽度
    */
    VGUScalar w;

    /**
    * 矩形高度
    */
    VGUScalar h;
} VGURect;

/**
 * @brief 图像滤波类型。
 *
 */
typedef enum {
    /**
    * 双线性插值(默认)
    */
    VGU_FILTER_BILINEAR = 0,

    /**
    * 无插值
    */
    VGU_FILTER_NEAREST,

    /**
    * 线性插值.
    */
    VGU_FILTER_LINEAR,

    /**
    * 无效定义
    */
    VGU_FILTER_BUTT
} VGUFilter;

/**
 * @brief 填充规则定义。
 *
 */
typedef enum {
    /**
    * 非0填充(默认)
    */
    VGU_RULE_WINDING = 0,

    /**
    * 奇偶填充
    */
    VGU_RULE_EVEN_ODD,

    /**
    * 无效定义
    */
    VGU_RULE_BUTT
} VGUFillRule;

/**
 * @brief 渐变填充区域外的延展类型。
 *
 */
typedef enum {
    /**
    * Pad类型(默认)
    */
    VGU_SPREAD_PAD = 0,

    /**
    * Reflect类型
    */
    VGU_SPREAD_REFLECT,

    /**
    * Repeat类型
    */
    VGU_SPREAD_REPEAT,

    /**
    * 无效定义
    */
    VGU_SPREAD_BUTT
} VGUFillSpread;

/**
 * @brief 图像模式填充延展类型。
 *
 */
typedef enum {
    /**
    * 反射类型
    */
    VGU_WRAP_REFLECT = 0,

    /**
    * 重复类型
    */
    VGU_WRAP_REPEAT,

    /**
    * 无效定义
    */
    VGU_WRAP_BUTT
} VGUWrapType;

/**
 * @brief 路径绘制指令类型。
 *
 */
typedef enum {
    /**
    * 线段关闭，坐标数据: none
    */
    VGU_PATH_CMD_CLOSE = 0,

    /**
    * 移动到指定位置，坐标数据: x0, y0
    */
    VGU_PATH_CMD_MOVE,

    /**
    * 线条，坐标数据: x0, y0
    */
    VGU_PATH_CMD_LINE,

    /**
    * 水平线，坐标数据: x0
    */
    VGU_PATH_CMD_HLINE,

    /**
    * 垂直线，坐标数据: y0
    */
    VGU_PATH_CMD_VLINE,

    /**
    * 二次贝塞尔曲线，坐标数据: x0, y0, x1, y1
    */
    VGU_PATH_CMD_QUAD,

    /**
    * 三次贝塞尔曲线，坐标数据: x0, y0, x1, y1, x2, y2
    */
    VGU_PATH_CMD_CUBIC,

    /**
    * 平滑二次贝塞尔曲线，坐标数据: x1, y1
    */
    VGU_PATH_CMD_SQUAD,

    /**
    * 平滑三次贝塞尔曲线，坐标数据: x1, y1, x2, y2
    */
    VGU_PATH_CMD_SCUBIC,

    /**
    * 无效定义
    */
    VGU_PATH_CMD_BUTT,
} VGUPathCmd;

/**
 * @brief 路径对象，存放路径命令和坐标数据。
 *
 */
typedef struct {
    /**
    * 存放路径命令数据
    */
    uint8_t *segment;

    /**
    * 路径命令总数
    */
    int32_t numSegments;

    /**
    * 存放路径命令对应坐标数据
    */
    uint8_t *data;

    /**
    * 路径数据存储类型
    */
    VGUPathDataType type;

    /**
    * 抗锯齿开关
    */
    bool enAlias;

    /**
    * 路径最大边界
    */
    VGURect boundBox;
} VGUPath;

/**
 * @brief 变换类型。
 *
 */
typedef enum {
    /**
    * 平移变换
    */
    VGU_TRANSFORM_TRANSLATE = (1 << 0),

    /**
    * 缩放变换
    */
    VGU_TRANSFORM_SCALE = (1 << 1),

    /**
    * 旋转90度
    */
    VGU_TRANSFORM_ROTATE_90 = (1 << 2),

    /**
    * 旋转180度
    */
    VGU_TRANSFORM_ROTATE_180 = (1 << 3),

    /**
    * 旋转270度
    */
    VGU_TRANSFORM_ROTATE_270 = (1 << 4),

    /**
    * 其他变换
    */
    VGU_TRANSFORM_OTHER = (1 << 16)
} VGUTransformType;

/**
 * @brief 变换矩阵。
 *
 */
typedef struct {
    /**
    * 3x3变换矩阵
    */
    float m[3][3];

    /**
    * 矩阵变换类型，简单变换为缩放，平移，90度倍数旋转
    */
    uint32_t type;
} VGUMatrix3;

/**
 * @brief 硬件加速渲染位图缓存。
 *
 */
typedef struct {
    /**
    * 像素格式
    */
    VGUPixelFormat pixelFormat;

    /**
    * 位图宽度
    */
    uint32_t width;

    /**
    * 位图高度
    */
    uint32_t height;

    /**
    * 位图stride
    */
    uint32_t stride;

    /**
    * 位图缓存的虚拟地址
    */
    void *virAddr;

    /**
    * 位图缓存的物理地址
    */
    uint64_t phyAddr;
} VGUBuffer;

/**
 * @brief 绘制表面剪切类型。
 *
 */
typedef enum {
    /**
    * 矩形剪切(默认)
    */
    VGU_CLIP_RECT = 0,

    /**
    * 路径剪切
    */
    VGU_CLIP_PATH,

    /**
    * 无效定义
    */
    VGU_CLIP_BUTT
} VGUClipType;

/**
 * @brief 定义蒙版图层。
 *
 */
typedef struct {
    /**
    * 蒙版缓存
    */
    VGUBuffer *buffer;

    /**
    * 蒙版矩形
    */
    VGURect *rect;
} VGUMaskLayer;

/**
 * @brief 2D硬件加速绘制目标表面。
 *
 */
typedef struct {
    /**
    * 位图缓存
    */
    VGUBuffer *buffer;
    union {
        /**
        * 矩形剪切域，如果为空，整个表面直接渲染
        */
        VGURect *clipRect;

        /**
        * 路径剪切域，如果为空，整个表面直接渲染
        */
        VGUPath *clipPath;
    };

    /**
    * 表面剪切类型
    */
    VGUClipType clipType;

    /**
    * 蒙版图层, 可以为空
    */
    VGUMaskLayer *mask;

    /**
    * 混合叠加模式
    */
    VGUBlendType blend;

    /**
    * 图像滤波类型
    */
    VGUFilter filter;
} VGUSurface;

/**
 * @brief 渐变颜色分布位置。
 *
 */
typedef struct {
    /**
    * 颜色偏移位置, 值范围是 0.0 ~ 1.0
    */
    float stop;

    /**
    * 偏移位置对应颜色
    */
    uint32_t color;
} VGUColorStop;

/**
 * @brief 线性渐变。
 *
 */
typedef struct {
    /**
    * 线性渐变起点水平坐标
    */
    VGUScalar x1;

    /**
    * 线性渐变起点垂直坐标
    */
    VGUScalar y1;

    /**
    * 线性渐变终点水平坐标
    */
    VGUScalar x2;

    /**
    * 线性渐变终点垂直坐标
    */
    VGUScalar y2;
} VGULinear;

/**
 * @brief 辐射渐变。
 *
 */
typedef struct {
    /**
    * 内圈圆心水平坐标
    */
    VGUScalar x0;

    /**
    * 内圈圆心垂直坐标
    */
    VGUScalar y0;

    /**
    * 内圈圆半径
    */
    VGUScalar r0;

    /**
    * 外圈圆心水平坐标
    */
    VGUScalar x1;

    /**
    * 外圈圆心垂直坐标
    */
    VGUScalar y1;

    /**
    * 外圈圆半径
    */
    VGUScalar r1;
} VGURadial;

/**
 * @brief 圆锥渐变。
 *
 */
typedef struct {
    /**
    * 圆弧中心x坐标
    */
    VGUScalar cx;

    /**
    * 圆弧中心y坐标
    */
    VGUScalar cy;
} VGUConic;

/**
 * @brief 图像对象。
 *
 */
typedef struct {
    /**
    * 图像存储缓存
    */
    VGUBuffer *buffer;

    /**
    * 图像矩阵变换，该参数为空，则内部使用单位变换矩阵
    */
    VGUMatrix3 *matrix;

    /**
    * 图像截取矩形，该参数为空，则截取整个图像
    */
    VGURect *rect;

    /**
    * 透明度，范围0~255
    */
    uint8_t opacity;
} VGUImage;

/**
 * @brief 图片模式对象。
 *
 */
typedef struct {
    /**
    * 图像对象
    */
    VGUImage *image;

    /**
    * 图像水平方向平铺类型
    */
    VGUWrapType wrapx;

    /**
    * 图像垂直方向平铺类型
    */
    VGUWrapType wrapy;
} VGUPattern;

/**
 * @brief 渐变类型。
 *
 */
typedef enum {
    /**
    * 线性渐变
    */
    VGU_GRADIENT_LINEAR = 0,

    /**
    * 辐射渐变
    */
    VGU_GRADIENT_RADIAL,

    /**
    * 圆锥渐变
    */
    VGU_GRADIENT_CONIC,

    /**
    * 无效定义
    */
    VGU_GRADIENT_BUTT
} VGUGradientType;

/**
 * @brief 渐变对象。
 *
 */
typedef struct {
    /**
    * 针对渐变对象的变换矩阵
    */
    VGUMatrix3 *matrix;

    /**
    * 渐变停止颜色数组指针
    */
    VGUColorStop *colorStops;

    /**
    * 渐变停止颜色个数
    */
    uint16_t stopCount;
    union {
        /**
        * 线性渐变对象
        */
        VGULinear linear;

        /**
        * 辐射渐变对象
        */
        VGURadial radial;

        /**
        * 圆锥渐变对象
        */
        VGUConic conic;
    };
    /**
    * 渐变类型
    */
    VGUGradientType type;

    /**
    * 渐变延伸模式
    */
    VGUFillSpread spread;

    /**
    * 透明度，范围0~255
    */
    uint8_t opacity;
} VGUGradient;

/**
 * @brief 颜色对象
 *
 */
typedef struct {
    /**
    * 颜色值
    */
    uint32_t color;

    /**
    * 透明度. 值范围 0 ~ 255.
    */
    uint8_t opacity;
} VGUSolid;

/**
 * @brief 渲染对象
 *
 */
typedef enum {
    /**
    * 填充颜色
    */
    VGU_PAINT_SOLID = 0,

    /**
    * 渲染渐变对象
    */
    VGU_PAINT_GRADIENT,

    /**
    * 渲染图片模式
    */
    VGU_PAINT_PATTERN,

    /**
    * 无效操作
    */
    VGU_PAINT_BUTT
} VGUPaintType;

/**
 * @brief 填充或描边路径的渲染风格。
 *
 */
typedef struct {
    union {
        /**
        * 渐变对象
        */
        VGUGradient *gradient;

        /**
        * 图片模式对象
        */
        VGUPattern *pattern;

        /**
        * 颜色对象
        */
        VGUSolid *solid;
    };
    /**
    * 渲染类型
    */
    VGUPaintType type;
} VGUPaintStyle;

/**
 * @brief 填充路径的属性。
 *
 */
typedef struct {
    /**
    * 填充规格
    */
    VGUFillRule rule;
} VGUFillAttr;

/**
 * @brief 描边路径的属性。
 *
 */
typedef struct {
    /**
    * 线帽类型
    */
    VGULineCap cap;

    /**
    * 联结类型
    */
    VGUJointType join;

    /**
    * 最大斜切长度
    */
    float miterLimit;

    /**
    * 线宽
    */
    float width;
} VGUStrokeAttr;

/**
 * @brief 定义2D硬件加速驱动函数。
 */
typedef struct {
    /**
     * @brief 初始化硬件加速。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @see DeinitVgu
     * @since 3.0
     */
    VGUResult (*InitVgu)(void);

    /**
     * @brief 去初始化硬件加速。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @see InitVgu
     * @since 3.0
     */
    VGUResult (*DeinitVgu)(void);

    /**
     * @brief 查询硬件能力集。
     *
     * @param cap 待查询能力。该参数类型定义参考 <b>VGUCapability</b>。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    int32_t (*QueryCapability)(uint32_t cap);

    /**
     * @brief 使用指定的渲染对象来填充路径。
     *
     * @param target 渲染目标表面。
     * @param path 路径对象。
     * @param matrix 变换矩阵对象. 如果该参数为空，默认为单位矩阵。
     * @param attr 填充属性。
     * @param style 绘制对象。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderFill)(VGUSurface *target, const VGUPath *path, const VGUMatrix3 *matrix, const VGUFillAttr *attr,
        const VGUPaintStyle *style);

    /**
     * @brief 使用指定的渲染对象来描边路径。
     *
     * @param target 渲染目标表面。
     * @param path 路径对象。
     * @param matrix 变换矩阵对象. 如果该参数为空，默认为单位矩阵。
     * @param attr 描边属性。
     * @param style 绘制对象。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderStroke)(VGUSurface *target, const VGUPath *path, const VGUMatrix3 *matrix,
        const VGUStrokeAttr *attr, const VGUPaintStyle *style);

    /**
     * @brief 对目标表面进行模糊处理。
     *
     * @param target 渲染目标表面。
     * @param blur 模糊半径。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderBlur)(VGUSurface *target, uint16_t blur);

    /**
     * @brief 对图像进行搬移操作。
     *
     * 搬移过程中同时进行颜色空间转换，矩阵变换操作。
     *
     * @param target 渲染目标表面。
     * @param src 待叠加源图像。
     * @param color 参与混合的颜色值。如果颜色值为0，则不参与混合操作
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderBlit)(VGUSurface *target, const VGUImage *src, uint32_t color);

    /**
     * @brief 对多个源图像进行叠加操作。
     *
     * 搬移过程中同时进行颜色空间转换，矩阵变换操作。该接口支持多个源图形同时叠加到目标表面，\n
     * 对于硬件支持源图像数目可以通过<b>QueryCapability<b/>接口查询
     *
     * @param target 渲染目标表面。
     * @param src 待叠加源图像数组。
     * @param count 待叠加源图像个数。
     * @param color 参与混合的颜色值。如果颜色值为0，则不参与混合操作
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderBlitN)(VGUSurface *target, const VGUImage *src, uint16_t count, uint32_t color);

    /**
     * @brief 对指定矩形进行颜色清除操作。
     *
     * @param target 渲染目标表面。
     * @param rect 待填充矩形大小，如果该参数为空，则整个表面清除。
     * @param color 填充颜色。
     * @param opacity 填充透明度。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderClearRect)(VGUSurface *target, const VGURect *rect, uint32_t color, uint8_t opacity);

    /**
     * @brief 取消硬件加速渲染。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderCancel)();

    /**
     * @brief 同步硬件加速模块绘制或搬移操作。
     *
     * 该函数将阻塞等待硬件绘制完成后继续运行。
     *
     * @param timeOut 该参数表示硬件加速同步等待超时时间。 值为<b>0</b>表示没有等待时间。
     *
     * @return Returns VGU_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
     *
     * @since 3.0
     */
    VGUResult (*RenderSync)(int32_t timeOut);
} VGUFuncs;

/**
 * @brief 初始化路径对象。
 *
 * @param path 路径对象。
 * @param type 存储路径的数据类型。
 * @param segments 路径的命令缓存。
 * @param numSegments 路径命令总数。
 * @param data 路径命令对应的坐标缓存。
 * @param enAlias 使能抗锯齿。
 * @param boundBox 路径的边界范围。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUPathInit(VGUPath *path, VGUPathDataType type, const uint8_t *segments, int numSegments,
    const uint8_t *data, bool enAlias, VGURect boundBox);

/**
 * @brief 添加子路径到当前路径中。
 *
 * @param path 路径对象。
 * @param subpath 存放子路径对象
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUPathAppend(VGUPath *path, const VGUPath *subpath);

/**
 * @brief 清除路径对象内存。
 *
 * @param path 路径对象。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUPathClear(VGUPath *path);

/**
 * @brief 初始化矩阵对象为单位矩阵。
 *
 * @param matrix 变换矩阵对象。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUMatrixIdentity(VGUMatrix3 *matrix);

/**
 * @brief 矩阵变换缩放。
 *
 * @param matrix 变换矩阵对象。
 * @param xScale 水平方向缩放倍数。
 * @param yScale 垂直方向缩放倍数。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUMatrixScale(VGUMatrix3 *matrix, float xScale, float yScale);

/**
 * @brief 矩阵变换旋转。
 *
 * @param matrix 变换矩阵对象。
 * @param degree 旋转度数。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUMatrixRotate(VGUMatrix3 *matrix, float degree);

/**
 * @brief 矩阵变换平移。
 *
 * @param matrix 变换矩阵对象。
 * @param x 水平方向位置。
 * @param y 垂直方向位置。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUMatrixTranslate(VGUMatrix3 *matrix, float x, float y);

/**
 * @brief 对渐变添加ColorStop。
 *
 * @param gradient 渐变对象。
 * @param colorStop ColorStop数组指针。
 * @param count colorStops总数。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientColorStop(VGUGradient *gradient, const VGUColorStop *colorStop, uint32_t count);

/**
 * @brief 清除ColorStop。
 *
 * @param gradient 渐变对象。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientClearStop(VGUGradient *gradient);

/**
 * @brief 设置渐变对象的变换矩阵。
 *
 * @param gradient 渐变对象。
 * @param matrix 渐变对象变换矩阵。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientMatrix(VGUGradient *gradient, const VGUMatrix3 *matrix);

/**
 * @brief 创建线性渐变对象。
 *
 * @param gradient 渐变对象。
 * @param p1 起点坐标。
 * @param p2 终点坐标。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientLinear(VGUGradient *gradient, const VGUPoint *p1, const VGUPoint *p2);

/**
 * @brief 创建辐射渐变对象
 *
 * @param gradient 渐变对象。
 * @param p1 内圆圆心坐标。
 * @param r1 内圆半径。
 * @param p2 外圆圆心坐标。
 * @param r2 外圆半径。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientRadial(VGUGradient *gradient, const VGUPoint *p1, VGUScalar r1, const VGUPoint *p2, VGUScalar r2);

/**
 * @brief 创建圆锥渐变对象。
 *
 * @param gradient 渐变对象。
 * @param cx 渐变中心水平坐标。
 * @param cy 渐变中心垂直坐标。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUGradientConic(VGUGradient *gradient, VGUScalar cx, VGUScalar cy);

/**
 * @brief 获取硬件加速相关的操作接口指针。
 *
 * @param funcs 硬件加速模块操作接口指针，调用者不需要分配内存，调用者获取该指针操作硬件加速。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUInitialize(VGUFuncs **funcs);

/**
 * @brief 去初始化硬件加速模块，同时释放硬件加速模块操作函数指针。
 *
 * @param funcs 硬件加速操作接口指针。
 *
 * @return Returns VGU_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link VGUResult}。
 *
 * @since 3.0
 */
VGUResult VGUUninitialize(VGUFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif