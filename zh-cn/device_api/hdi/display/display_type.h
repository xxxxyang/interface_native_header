/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file display_type.h
 *
 * @brief 显示类型定义，定义显示驱动接口所使用的数据类型。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef DISPLAY_TYPE_H
#define DISPLAY_TYPE_H
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 返回值类型定义。
 *
 */
typedef enum {
    /**
    * 成功
    */
    DISPLAY_SUCCESS = 0,

    /**
    * 失败
    */
    DISPLAY_FAILURE = -1,

    /**
    * fd错误
    */
    DISPLAY_FD_ERR = -2,

    /**
    * 参数错误
    */
    DISPLAY_PARAM_ERR = -3,

    /**
    * 空指针
    */
    DISPLAY_NULL_PTR = -4,

    /**
    * 不支持的特性
    */
    DISPLAY_NOT_SUPPORT = -5,

    /**
    * 内存不足
    */
    DISPLAY_NOMEM = -6,

    /**
    * 系统繁忙
    */
    DISPLAY_SYS_BUSY = -7,

    /**
    * 操作不允许
    */
    DISPLAY_NOT_PERM = -8
} DispErrCode;

/**
 * @brief 图层类型定义。
 *
 */
typedef enum {
    /**
    * 图形层
    */
    LAYER_TYPE_GRAPHIC,

    /**
    * 视频层
    */
    LAYER_TYPE_OVERLAY,

    /**
    * 媒体播放
    */
    LAYER_TYPE_SDIEBAND,

    /**
    * 空图层
    */
    LAYER_TYPE_BUTT
} LayerType;

/**
 * @brief 像素格式类型定义。
 *
 */
typedef enum {
    /**
    * CLUT8 格式
    */
    PIXEL_FMT_CLUT8 = 0,

    /**
    * CLUT1 格式
    */
    PIXEL_FMT_CLUT1,

    /**
    * CLUT4 格式
    */
    PIXEL_FMT_CLUT4,

    /**
    * RGB565 格式
    */
    PIXEL_FMT_RGB_565,

    /**
    * RGBA5658 格式
    */
    PIXEL_FMT_RGBA_5658,

    /**
    * RGBX4444 格式
    */
    PIXEL_FMT_RGBX_4444,

    /**
    * RGBA4444 格式
    */
    PIXEL_FMT_RGBA_4444,

    /**
    * RGB444 格式
    */
    PIXEL_FMT_RGB_444,

    /**
    * RGBX5551 格式
    */
    PIXEL_FMT_RGBX_5551,

    /**
    * RGBA5551 格式
    */
    PIXEL_FMT_RGBA_5551,

    /**
    * RGB555 格式
    */
    PIXEL_FMT_RGB_555,

    /**
    * RGBX8888 格式
    */
    PIXEL_FMT_RGBX_8888,

    /**
    * RGBA8888 格式
    */
    PIXEL_FMT_RGBA_8888,

    /**
    * RGB888 格式
    */
    PIXEL_FMT_RGB_888,

    /**
    * BGR565 格式
    */
    PIXEL_FMT_BGR_565,

    /**
    * BGRX4444 格式
    */
    PIXEL_FMT_BGRX_4444,

    /**
    * BGRA4444 格式
    */
    PIXEL_FMT_BGRA_4444,

    /**
    * BGRX5551 格式
    */
    PIXEL_FMT_BGRX_5551,

    /**
    * BGRA5551 格式
    */
    PIXEL_FMT_BGRA_5551,

    /**
    * BGRX8888 格式
    */
    PIXEL_FMT_BGRX_8888,

    /**
    * BGRA8888 格式
    */
    PIXEL_FMT_BGRA_8888,

    /**
    * YUV422 交错格式
    */
    PIXEL_FMT_YUV_422_I,

    /**
    * YCBCR422 半平面格式
    */
    PIXEL_FMT_YCBCR_422_SP,

    /**
    * YCRCB422 半平面格式
    */
    PIXEL_FMT_YCRCB_422_SP,

    /**
    * YCBCR420 半平面格式
    */
    PIXEL_FMT_YCBCR_420_SP,

    /**
    * YCRCB420 半平面格式
    */
    PIXEL_FMT_YCRCB_420_SP,

    /**
    * YCBCR422 平面格式
    */
    PIXEL_FMT_YCBCR_422_P,

    /**
    * YCRCB422 平面格式
    */
    PIXEL_FMT_YCRCB_422_P,

    /**
    * YCBCR420 平面格式
    */
    PIXEL_FMT_YCBCR_420_P,

    /**
    * YCRCB420 平面格式
    */
    PIXEL_FMT_YCRCB_420_P,

    /**
    * YUYV422 打包格式
    */
    PIXEL_FMT_YUYV_422_PKG,

    /**
    * UYVY422 打包格式
    */
    PIXEL_FMT_UYVY_422_PKG,

    /**
    * YVYU422 打包格式
    */
    PIXEL_FMT_YVYU_422_PKG,

    /**
    * VYUY422 打包格式
    */
    PIXEL_FMT_VYUY_422_PKG,

    /**
    * 无效像素格式
    */
    PIXEL_FMT_BUTT
} PixelFormat;

/**
 * @brief 图层变换类型定义。
 *
 */
typedef enum {
    /**
    * 不旋转
    */
    ROTATE_NONE = 0,

    /**
    * 旋转90度
    */
    ROTATE_90,

    /**
    * 旋转180度
    */
    ROTATE_180,

    /**
    * 旋转270度
    */
    ROTATE_270,

    /**
    * 无效操作
    */
    ROTATE_BUTT
} TransformType;

/**
 * @brief 显示内存类型定义。
 *
 * 根据图形系统指定的类型进行分配，包括带cache或者不带cache的内存。
 *
 */
typedef enum {
    /**
    * 常规内存，不带cache
    */
    NORMAL_MEM = 0,

    /**
    * 带cache内存
    */
    CACHE_MEM,

    /**
    * 共享内存
    */
    SHM_MEM
} MemType;

/**
 * @brief 定义图像的压缩类型。
 *
 * 在硬件加速时可指定压缩类型，硬件加速按照指定的压缩类型进行压缩图像。
 *
 */
typedef enum {
    /**
    * No 混合操作
    */
    BLEND_NONE = 0,

    /**
    * CLEAR 混合操作
    */
    BLEND_CLEAR,

    /**
    * SRC 混合操作
    */
    BLEND_SRC,

    /**
    * SRC_OVER 混合操作
    */
    BLEND_SRCOVER,

    /**
    * DST_OVER 混合操作
    */
    BLEND_DSTOVER,

    /**
    * SRC_IN 混合操作
    */
    BLEND_SRCIN,

    /**
    * DST_IN 混合操作
    */
    BLEND_DSTIN,

    /**
    * SRC_OUT 混合操作
    */
    BLEND_SRCOUT,

    /**
    * DST_OUT 混合操作
    */
    BLEND_DSTOUT,

    /**
    * SRC_ATOP 混合操作
    */
    BLEND_SRCATOP,

    /**
    * DST_ATOP 混合操作
    */
    BLEND_DSTATOP,

    /**
    * ADD 混合操作
    */
    BLEND_ADD,

    /**
    * XOR 混合操作
    */
    BLEND_XOR,

    /**
    * DST 混合操作
    */
    BLEND_DST,

    /**
    * AKS 混合操作
    */
    BLEND_AKS,

    /**
    * AKD 混合操作
    */
    BLEND_AKD,

    /**
    * 空操作
    */
    BLEND_BUTT
} BlendType;

/**
 * @brief 硬件加速支持的ROP操作类型。
 *
 * 硬件加速支持的ROP操作类型，在将前景位图的RGB颜色分量和Alpha分量值与背景位图的RGB颜色
 * 分量值和Alpha分量值进行按位的布尔运算（包括按位与，按位或等），将结果输出。
 *
 */
typedef enum {
    /**
    * Blackness
    */
    ROP_BLACK = 0,

    /**
    *~(S2+S1)
    */
    ROP_NOTMERGEPEN,

    /**
    * ~S2&S1
    */
    ROP_MASKNOTPEN,

    /**
    *~S2
    */
    ROP_NOTCOPYPEN,

    /**
    * S2&~S1
    */
    ROP_MASKPENNOT,

    /**
    * ~S1
    */
    ROP_NOT,

    /**
    * S2^S1
    */
    ROP_XORPEN,

    /**
    * ~(S2&S1)
    */
    ROP_NOTMASKPEN,

    /**
    * S2&S1
    */
    ROP_MASKPEN,

    /**
    * ~(S2^S1)
    */
    ROP_NOTXORPEN,

    /**
    * S1
    */
    ROP_NOP,

    /**
    * ~S2+S1
    */
    ROP_MERGENOTPEN,

    /**
    * S2
    */
    ROP_COPYPE,

    /**
    * S2+~S1
    */
    ROP_MERGEPENNOT,

    /**
    * S2+S1
    */
    ROP_MERGEPEN,

    /**
    * Whiteness
    */
    ROP_WHITE,

    /**
    * Invalid ROP type
    */
    ROP_BUTT
} RopType;

/**
 * @brief Color key操作类型定义，即硬件加速支持的Color key操作类型。
 *
 */
typedef enum {
    /**
    * 不使用colorkey
    */
    CKEY_NONE = 0,

    /**
    * 使用源colorkey
    */
    CKEY_SRC,

    /**
    * 使用目标colorkey
    */
    CKEY_DST,

    /**
    * 空操作
    */
    CKEY_BUTT
} ColorKey;

/**
 * @brief 硬件加速支持的镜像操作类型定义
 *
 */
typedef enum {
    /**
    * 不使用镜像
    */
    MIRROR_NONE = 0,

    /**
    * 左右镜像
    */
    MIRROR_LR,

    /**
    * 上下镜像
    */
    MIRROR_TB,

    /**
    * 空操作
    */
    MIRROR_BUTT
} MirrorType;

/**
 * @brief 热插拔连接类型定义
 *
 */
typedef enum {
    /**
    * 无效类型
    */
    INVALID = 0,

    /**
    * 已连接
    */
    CONNECTED,

    /**
    * 断开连接
    */
    DISCONNECTED
} Connection;

/**
 * @brief 定义显示信息结构体
 *
 */
typedef struct {
    /**
    * 显示屏宽度
    */
    uint32_t width;

    /**
    * 显示屏高度
    */
    uint32_t height;

    /**
    * 显示屏旋转角度
    */
    int32_t rotAngle;
} DisplayInfo;

/**
 * @brief 定义图层信息结构体
 *
 * 在创建图层时，需要将LayerInfo传递给创建图层接口，创建图层接口根据图层信息创建相应图层。
 *
 */
typedef struct {
    /**
    * 图层宽度
    */
    int32_t width;

    /**
    * 图层高度
    */
    int32_t height;

    /**
    * 图层类型，包括图形层、视频层和媒体播放模式
    */
    LayerType type;

    /**
    * 每像素所占bit数
    */
    int32_t bpp;

    /**
    * 图层像素格式
    */
    PixelFormat pixFormat;
} LayerInfo;

/**
 * @brief 定义图层Alpha信息的结构体
 *
 */
typedef struct {
    /**
    * 全局alpha使能标志
    */
    bool enGlobalAlpha;

    /**
    * 像素alpha使能标志
    */
    bool enPixelAlpha;

    /**
    * alpha0值，取值范围：[0, 255]
    */
    uint8_t alpha0;

    /**
    * alpha1值，取值范围：[0, 255]
    */
    uint8_t alpha1;

    /**
    * 全局alpha值，取值范围：[0, 255]
    */
    uint8_t gAlpha;
} LayerAlpha;

/**
 * @brief buffer句柄的定义。
 *
 * 包括共享内存键值、共享内存标识、物理内存地址。
 *
 */
typedef struct {
    /**
    * 共享内存键值
    */
    int32_t key;

    /**
    * 共享内存唯一标识
    */
    int32_t shmid;

    /**
    * 物理内存地址
    */
    uint64_t phyAddr;
} BufferHandle;

/**
 * @brief 显示内存buffer结构体定义，包括内存物理地址，内存虚拟地址等。
 *
 */
typedef struct {
    /**
    * buffer句柄
    */
    BufferHandle hdl;

    /**
    * 申请的内存类型
    */
    MemType type;

    /**
    * 申请的内存大小
    */
    uint32_t size;

    /**
    * 申请的内存虚拟地址
    */
    void *virAddr;
} GrallocBuffer;

/**
 * @brief 图层buffer结构体定义，包括虚拟内存地址和物理内存地址。
 *
 */
typedef struct {
    /**
    * 物理内存地址
    */
    uint64_t phyAddr;

    /**
    * 虚拟内存地址
    */
    void *virAddr;
} BufferData;

/**
 * @brief 图层Buffer，用于存放图层数据。
 *
 */
typedef struct {
    /**
    * buffer 的fence号
    */
    int32_t fenceId;

    /**
    * buffer宽度
    */
    int32_t width;

    /**
    * buffer高度
    */
    int32_t height;

    /**
    * 一行数据所占字节数
    */
    int32_t pitch;

    /**
    * buffer像素格式
    */
    PixelFormat pixFormat;

    /**
    * 图层buffer数据
    */
    BufferData data;
} LayerBuffer;

/**
 * @brief 定义矩形信息
 *
 */
typedef struct {
    /**
    * 矩形框起始x坐标
    */
    int32_t x;

    /**
    * 矩形框起始y坐标
    */
    int32_t y;

    /**
    * 矩形框宽度
    */
    int32_t w;

    /**
    * 矩形框高度
    */
    int32_t h;
} IRect;

/**
 * @brief 用于存放窗口相关信息的结构体定义，提供给硬件加速使用，例如图像合成，位图搬移等操作。
 */
typedef struct {
    /**
    * 图像首地址
    */
    uint64_t phyAddr;

    /**
    * 图像高度
    */
    int32_t height;

    /**
    * 图像宽度
    */
    int32_t width;

    /**
    * 图像跨度
    */
    int32_t stride;

    /**
    * 图像格式
    */
    PixelFormat enColorFmt;

    /**
    * CLUT表是否位于 YCbCr 空间
    */
    bool bYCbCrClut;

    /**
    * 图像alpha最大值为255还是128
    */
    bool bAlphaMax255;

    /**
    * 是否使能1555的Alpha扩展
    */
    bool bAlphaExt1555;

    /**
    * Alpha0值，取值范围：[0,255]
    */
    uint8_t alpha0;

    /**
    * Alpha1值，取值范围：[0,255]
    */
    uint8_t alpha1;

    /**
    * CbCr分量地址
    */
    uint64_t cbcrPhyAddr;

    /**
    * CbCr分量跨度
    */
    int32_t cbcrStride;

    /**
    * Clut表首地址，用作颜色扩展或颜色校正
    */
    uint64_t clutPhyAddr;
} ISurface;

/**
 * @brief 线条描述结构体定义，用于硬件加速绘制直线。
 *
 */
typedef struct {
    /**
    * 线条起点的x坐标
    */
    int32_t x0;

    /**
    * 线条起点的y坐标
    */
    int32_t y0;

    /**
    * 线条终点的x坐标
    */
    int32_t x1;

    /**
    * 线条终点的y坐标
    */
    int32_t y1;

    /**
    * 线条颜色
    */
    uint32_t color;
} ILine;

/**
 * @brief 圆形描述结构体定义，用于硬件加速绘制圆形。
 *
 */
typedef struct {
    /**
    * 圆心x坐标
    */
    int32_t x;

    /**
    * 圆心y坐标
    */
    int32_t y;

    /**
    * 圆的半径
    */
    int32_t r;

    /**
    * 圆的颜色
    */
    uint32_t color;
} ICircle;

/**
 * @brief 矩形描述结构体定义，用于硬件加速绘制矩形，
 *
 */
typedef struct {
    /**
    * 矩形区域
    */
    IRect rect;

    /**
    * 矩形颜色
    */
    uint32_t color;
} Rectangle;

/**
 * @brief 图像硬件加速相关的操作选项结构体定义，用于图像硬件加速时的操作选项。
 *
 */
typedef struct {
    /**
    * 全局alpha使能位
    */
    bool enGlobalAlpha;

    /**
    * 全局alpha的值
    */
    uint32_t globalAlpha;

    /**
    * 像素alpha使能位
    */
    bool enPixelAlpha;

    /**
    * 混合方式
    */
    BlendType blendType;

    /**
    * 色键模式
    */
    ColorKey colorKeyFrom;

    /**
    * Rop功能使能位
    */
    bool enableRop;

    /**
    * 颜色的Rop类型
    */
    RopType colorRopType;

    /**
    * Alpha的Rop类型
    */
    RopType alphaRopType;

    /**
    * 缩放功能使能位
    */
    bool enableScale;

    /**
    * 旋转类型
    */
    TransformType rotateType;

    /**
    * 镜像类型
    */
    MirrorType mirrorType;
} GfxOpt;

#ifdef __cplusplus
}
#endif
#endif
/* @} */