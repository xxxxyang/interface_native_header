/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 * @since 1.0
 * @version 2.0
 */

 /**
 * @file display_device.h
 *
 * @brief 显示设备控制接口声明。
 *
 * @since 1.0
 * @version 2.0
 */

#ifndef DISPLAY_DEVICE_H
#define DISPLAY_DEVICE_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 显示设备控制接口结构体，定义显示设备控制接口函数指针。
 */
typedef struct {
    /**
     * @brief 设置电源状态
     *
     * 在系统休眠或者唤醒时，display服务或电源管理模块设置电源状态，以使驱动IC能正常进入对应的模式。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param status 设置的电源状态，display服务控制显示设备进入on、off等状态{具体参考@link PowerStatus}。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayPowerStatus)(uint32_t devId, PowerStatus status);

    /**
     * @brief 获取电源状态
     *
     * 获取devId对应设备的电源状态
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param status 获取到的devId对应设备的电源状态{具体参考@link PowerStatus}。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayPowerStatus)(uint32_t devId, PowerStatus *status);

    /**
     * @brief 设置背光等级
     *
     * 设置devId对应设备的背光等级
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param level 将要设置的具体背光值
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayBacklight)(uint32_t devId, uint32_t level);

    /**
     * @brief 获取背光等级
     *
     * 获取devId对应设备的背光等级
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备。
     * @param level 获取到的具体背光值。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayBacklight)(uint32_t devId, uint32_t *level);
} DeviceFuncs;

/**
 * @brief 实现显示设备控制接口的初始化，申请操作显示设备控制接口的资源，并获取对应的操作接口
 *
 * @param funcs 显示设备控制接口指针，初始化时分配内存，调用者不需要分配内存，调用者获取该指针用于操作显示设备。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * otherwise.
 * @since 1.0
 * @version 1.0
 */
int32_t DeviceInitialize(DeviceFuncs **funcs);

/**
 * @brief 取消显示设备控制接口的初始化，释放控制接口使用到的资源。
 *
 * @param funcs 显示设备控制接口指针，用于释放初始化函数中分配的操作指针内存。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t DeviceUninitialize(DeviceFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif
/* @} */