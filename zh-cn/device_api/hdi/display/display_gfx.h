/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file display_gfx.h
 *
 * @brief 显示硬件加速驱动接口声明。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef DISPLAY_GFX_H
#define DISPLAY_GFX_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 显示硬件加速驱动接口结构体，定义硬件加速驱动接口函数指针。
 */
typedef struct {
    /**
     * @brief 初始化硬件加速。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see DeinitGfx
     * @since 1.0
     * @version 1.0
     */
    int32_t (*InitGfx)(void);

    /**
     * @brief 去初始化硬件加速。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see InitGfx
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DeinitGfx)(void);

    /**
     * @brief 填充矩形，用一种颜色填充画布上指定矩形区域的矩形框。
     *
     * @param surface 画布。
     * @param rect 填充的矩形区域。
     * @param color 填充的颜色。
     * @param opt 硬件加速选项。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*FillRect)(ISurface *surface, IRect *rect, uint32_t color, GfxOpt *opt);

    /**
     * @brief 绘制矩形框，用一种颜色在画布上绘制指定区域的矩形框。
     *
     * @param surface 画布。
     * @param rect 矩形框结构。
     * @param color 绘制的颜色。
     * @param opt 硬件加速选项。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DrawRectangle)(ISurface *surface, Rectangle *rect, uint32_t color, GfxOpt *opt);

    /**
     * @brief 绘制直线，使用一种颜色在画布上绘制一条直线。
     *
     * @param surface 画布。
     * @param line 直线结构。
     * @param opt 硬件加速选项。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DrawLine)(ISurface *surface, ILine *line, GfxOpt *opt);

    /**
     * @brief 绘制圆形，使用一种颜色在画布上绘制指定圆心和半径的圆。
     *
     * @param surface 画布。
     * @param circle 圆形结构。
     * @param opt 硬件加速选项。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DrawCircle)(ISurface *surface, ICircle *circle, GfxOpt *opt);

    /**
     * @brief 位图搬移
     *
     * 在位图搬移过程中，可以实现色彩空间转换、缩放、旋转等功能。
     *
     * @param srcSurface 源位图信息。
     * @param srcRect 源位图搬移区域。
     * @param dstSurface 目标位图信息。
     * @param dstRect 目标位图区域。
     * @param opt 硬件加速选项。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*Blit)(ISurface *srcSurface, IRect *srcRect, ISurface *dstSurface, IRect *dstRect, GfxOpt *opt);

    /**
     * @brief 硬件加速同步。
     *
     * 在使用硬件加速模块进行图像绘制、图像叠加、图像搬移时，通过调用该接口进行硬件同步，该接口会等待硬件加速完成。
     *
     * @param timeOut 硬件加速同步超时设置，设置为0表示无超时，等待直到硬件加速完成。
     *
     * @return Returns DISPLAY_SUCCESS 表示执行成功。
     * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*Sync)(int32_t timeOut);
} GfxFuncs;

/**
 * @brief 获取硬件加速相关的操作接口指针。
 *
 * @param funcs 硬件加速模块操作接口指针，调用者不需要分配内存，调用者获取该指针操作硬件加速。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GfxInitialize(GfxFuncs **funcs);

/**
 * @brief 释放硬件加速相关的操作接口指针。
 *
 * @param funcs 硬件加速操作接口指针。
 *
 * @return Returns DISPLAY_SUCCESS 表示执行成功。
 * @return Returns 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GfxUninitialize(GfxFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif
/** @} */