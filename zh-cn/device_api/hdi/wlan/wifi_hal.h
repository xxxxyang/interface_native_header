/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN可实现跨操作系统迁移，自适应器件差异，模块化拼装编译。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal.h
 *
 * @brief 提供给wifi service的WLAN基本能力接口。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_H
#define WIFI_HAL_H

#include "wifi_hal_ap_feature.h"
#include "wifi_hal_sta_feature.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 定义IWiFi回调函数的原型，监听异步事件。
 *
 * @param event 回调传入的事件类型标识
 * @param data 回调传入的数据
 * @param ifName 网卡名字
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
typedef int32_t (*CallbackFunc)(uint32_t event, void *data, const char *ifName);

/**
 * @brief HAL对wifi service提供的基本能力。
 *
 * 用于创建HAL与驱动的通道，创建/获取/销毁WLAN特性等。
 *
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFi {
    /**
     * @brief 创建HAL和驱动之间的通道及获取驱动网卡信息。
     *
     * @param iwifi IWiFi对象{@link IWiFi}
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*start)(struct IWiFi *iwifi);

    /**
     * @brief 销毁通道。
     *
     * @param iwifi IWiFi对象{@link IWiFi}
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*stop)(struct IWiFi *iwifi);

    /**
     * @brief 获取该设备支持的WLAN特性（不考虑当前的使用状态）。
     *
     *
     * @param supType 保存当前设备支持的特性
     * @param size supType数组的长度
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getSupportFeature)(uint8_t *supType, uint32_t size);

    /**
     * @brief 获取多网卡共存情况。
     *
     *
     * @param combo 基于芯片的能力保存当前所有支持的多网卡共存情况（比如支持AP，STA，P2P等不同组合的共存）
     * @param size combo数组的长度
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getSupportCombo)(uint64_t *combo, uint32_t size);

    /**
     * @brief 根据输入类型创建对应的特性{@link IWiFiBaseFeature}。
     *
     * @param type 创建的feature类型
     * @param ifeature 获取创建的feature对象
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*createFeature)(int32_t type, struct IWiFiBaseFeature **ifeature);

    /**
     * @brief 通过网络接口名字获取对应的特性。
     *
     * @param ifName 网络接口的名字
     * @param ifeature 获取该网络接口名字的feature对象
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getFeatureByIfName)(const char *ifName, struct IWiFiBaseFeature **ifeature);

    /**
     * @brief 注册IWiFi的回调函数，监听异步事件。
     *
     * @param cbFunc 注册的回调函数
     * @param ifName 网卡名字
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*registerEventCallback)(CallbackFunc cbFunc, const char *ifName);

    /**
     * @brief 去注册IWiFi的回调函数。
    
     * @param cbFunc 去注册的回调函数
     * @param ifName 网卡名字
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*unregisterEventCallback)(CallbackFunc cbFunc, const char *ifName);

    /**
     * @brief 销毁对应的特性。
     *
     * @param ifeature 销毁的feature对象
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*destroyFeature)(struct IWiFiBaseFeature *ifeature);

    /**
     * @brief WLAN驱动进行重置。
     *
     * @param chipId 驱动进行重置对应的芯片ID
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*resetDriver)(const uint8_t chipId, const char *ifName);

    /**
     * @brief 获取网络设备信息
     *
     * @param netDeviceInfoResult 得到的网络设备信息
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getNetDevInfo)(struct NetDeviceInfoResult *netDeviceInfoResult);

    /**
     * @brief 获取正在使用的功率模式
     *
     * @param ifName 网卡名字
     * @param mode 功率模式（包含睡眠模式、一般模式、穿墙模式）
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getPowerMode)(const char *ifName, uint8_t *mode);

    /**
     * @brief 设置功率模式
     *
     * @param ifName 网卡名字
     * @param mode 功率模式（包含睡眠模式、一般模式、穿墙模式）
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setPowerMode)(const char *ifName, uint8_t mode);
};

/**
 * @brief 创建IWiFi结构体，挂接{@link IWiFi}中能力接口。
 *
 * @param wifiInstance HAL服务对象{@link IWiFi}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t WifiConstruct(struct IWiFi **wifiInstance);

/**
 * @brief 销毁IWiFi结构体。
 *
 * @param wifiInstance HAL服务对象{@link IWiFi}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t WifiDestruct(struct IWiFi **wifiInstance);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
