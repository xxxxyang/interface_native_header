/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN可实现跨操作系统迁移，自适应器件差异，模块化拼装编译。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal_sta_feature.h
 *
 * @brief 提供WLAN的sta特性能力。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_STA_FEATURE_H
#define WIFI_HAL_STA_FEATURE_H

#include "wifi_hal_base_feature.h"
#include "wifi_driver_client.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 继承了{@link IWiFiBaseFeature}基本特性，额外包含设置扫描单个MAC地址功能。
 *
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFiSta {
    /**< 基本特性{@link IWiFiBaseFeature} */
    struct IWiFiBaseFeature baseFeature;

    /**
     * @brief 设置扫描单个MAC地址。
     *
     * @param staFeature STA特性{@link IWiFiSta}
     * @param scanMac 设置STA扫描的MAC地址
     * @param len MAC地址的长度
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setScanningMacAddress)(const struct IWiFiSta *staFeature, unsigned char *scanMac, uint8_t len);

    /**
     * @brief 启动扫描
     *
     * @param ifName 网卡名字
     * @param scan 扫描参数
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*startScan)(const char *ifName, WifiScan *scan);
};

/**
 * @brief 初始化STA特性。wifi service在创建STA类型的特性{@link FeatureType}时调用。
 *
 * @param fe STA特性{@link IWiFiSta}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t InitStaFeature(struct IWiFiSta **fe);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
