/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN 可实现跨操作系统迁移，自适应器件差异，模块化拼装编译。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal_base_feature.h
 *
 * @brief 提供WLAN基本特性能力。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_BASE_FEATURE_H
#define WIFI_HAL_BASE_FEATURE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 网卡名称最大长度。
 *
 * @since 1.0
 * @version 1.0
 */
#define IFNAME_MAX_LEN 16
/**
 * @brief WLAN的MAC地址长度。
 *
 * @since 1.0
 * @version 1.0
 */
#define WIFI_MAC_ADDR_LENGTH 6
/**
 * @brief 访问失败。
 *
 * @since 1.0
 * @version 1.0
 */
#define ERR_UNAUTH_ACCESS (-6)

/**
 * @brief 特性的类型{@link FeatureType}。
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    /**< 未定义的类型 */
    PROTOCOL_80211_IFTYPE_UNSPECIFIED,
    /**< 特设型网络 */
    PROTOCOL_80211_IFTYPE_ADHOC,
    /**< 工作站 */
    PROTOCOL_80211_IFTYPE_STATION,
    /**< 接入点 */
    PROTOCOL_80211_IFTYPE_AP,
    /**< 虚拟接入点 */
    PROTOCOL_80211_IFTYPE_AP_VLAN,
    /**< 无线分布式系统 */
    PROTOCOL_80211_IFTYPE_WDS,
    /**< 网络监听器 */
    PROTOCOL_80211_IFTYPE_MONITOR,
    /**< 组网 */
    PROTOCOL_80211_IFTYPE_MESH_POINT,
    /**< 对等网络客户端 */
    PROTOCOL_80211_IFTYPE_P2P_CLIENT,
    /**< 对等网络群组所有者 */
    PROTOCOL_80211_IFTYPE_P2P_GO,
    /**< 对等网络设备 */
    PROTOCOL_80211_IFTYPE_P2P_DEVICE,
     /**< 网口的数目 */
    PROTOCOL_80211_IFTYPE_NUM,
} FeatureType;

/**
 * @brief 基本特性，包含获取网络接口名称，设置MAC地址，设置发射功率等公共能力接口。
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFiBaseFeature {
    /**< 网卡名称 */
    char ifName[IFNAME_MAX_LEN];
    /**< 特性的类型{@link FeatureType} */
    int32_t type;

    /**
     * @brief 获取网络接口的名字。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    const char *(*getNetworkIfaceName)(const struct IWiFiBaseFeature *baseFeature);

    /**
     * @brief 获取特性的类型。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getFeatureType)(const struct IWiFiBaseFeature *baseFeature);

    /**
     * @brief 设置MAC地址。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     * @param mac 设置的MAC地址
     * @param len 设置的MAC地址长度
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setMacAddress)(const struct IWiFiBaseFeature *baseFeature, unsigned char *mac, uint8_t len);

    /**
     * @brief 获取设备的MAC地址。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     * @param mac 获得的MAC地址
     * @param len 获得的MAC地址长度
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getDeviceMacAddress)(const struct IWiFiBaseFeature *baseFeature, unsigned char *mac, uint8_t len);

    /**
     * @brief 获取指定频段（2.4G或者5G）下支持的频率。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     * @param band 指定的一个频段
     * @param freqs 保存支持的频率
     * @param count 频率数组的元素个数
     * @param num 实际支持的频率个数
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getValidFreqsWithBand)(const struct IWiFiBaseFeature *baseFeature, int32_t band, int32_t *freqs,
        uint32_t count, uint32_t *num);

    /**
     * @brief 设置发射功率。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     * @param power 设置的发射功率
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setTxPower)(const struct IWiFiBaseFeature *baseFeature, int32_t power);

    /**
     * @brief 获得当前驱动的芯片ID。
     *
     * @param baseFeature 基本特性{@link IWiFiBaseFeature}
     * @param chipId 获得的芯片ID
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getChipId)(const struct IWiFiBaseFeature *baseFeature, uint8_t *chipId);

    /**
     * @brief 通过芯片ID获得当前芯片所有的网卡名。
     *
     * @param chipId 驱动进行重置对应的芯片ID
     * @param ifNames 网卡名字
     * @param num 网卡的数量
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getIfNamesByChipId)(const uint8_t chipId, char **ifNames, uint32_t *num);
};

/**
 * @brief 初始化基本特性。用于wifi service在创建任何类型的特性{@link FeatureType}时。
 *
 * @param fe 基本特性{@link IWiFiBaseFeature}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t InitBaseFeature(struct IWiFiBaseFeature **fe);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
