/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Codec模块接口定义。
 *
 * Codec模块涉及自定义类型、音视频编解码组件初始化、参数设置、数据的轮转和控制等。
 *
 * @since 3.1
 * @version 2.0
 */

/**
 * @file codec_common_type.h
 *
 * @brief Codec模块接口定义中使用的自定义数据类型。
 *
 * Codec模块接口定义中使用的自定义数据类型，包括编解码类型、音视频参数、buffer定义等。
 *
 * @since 3.1
 * @version 2.0
 */

#ifndef CODEC_COMMON_TYPE_H
#define CODEC_COMMON_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 枚举编解码的类型
 */
typedef enum {
    VIDEO_DECODER,    /**< 视频解码类型 */
    VIDEO_ENCODER,    /**< 视频编码类型 */
    AUDIO_DECODER,    /**< 音频解码类型 */
    AUDIO_ENCODER,    /**< 音频编码类型 */
    INVALID_TYPE,     /**< 无效类型 */
} CodecType;

/**
 * @brief 枚举Codec规格.
 */
typedef enum {
    INVALID_PROFILE = 0,              /**< 无效的规格 */
    AAC_LC_PROFILE = 0x1000,          /**< AAC低复杂度规格 */
    AAC_MAIN_PROFILE,                 /**< AAC主规格 */
    AAC_HE_V1_PROFILE,                /**< AAC高效率和频带重现规格，又称为HEAAC，AAC+，或者AACPlusV1 */
    AAC_HE_V2_PROFILE,                /**< AAC高效率和频带重现以及变量立体声规格，又称为AAC++或者AACPlusV2 */
    AAC_LD_PROFILE,                   /**< AAC低延迟规格 */
    AAC_ELD_PROFILE,                  /**< AAC增强型低延迟规格 */
    AVC_BASELINE_PROFILE = 0x2000,    /**< H.264低规格 */
    AVC_MAIN_PROFILE,                 /**< H.264主规格 */
    AVC_HIGH_PROFILE,                 /**< H.264高规格 */
    HEVC_MAIN_PROFILE = 0x3000,       /**< H.265主规格 */
    HEVC_MAIN_10_PROFILE,             /**< H.265 10比特主规格 */
} Profile;

/**
 * @brief 枚举音频采样率
 */
typedef enum {
    AUD_SAMPLE_RATE_8000   = 8000,     /**< 8K采样率 */
    AUD_SAMPLE_RATE_12000  = 12000,    /**< 12K采样率 */
    AUD_SAMPLE_RATE_11025  = 11025,    /**< 11.025K采样率 */
    AUD_SAMPLE_RATE_16000  = 16000,    /**< 16K采样率 */
    AUD_SAMPLE_RATE_22050  = 22050,    /**< 22.050K采样率 */
    AUD_SAMPLE_RATE_24000  = 24000,    /**< 24K采样率 */
    AUD_SAMPLE_RATE_32000  = 32000,    /**< 32K采样率 */
    AUD_SAMPLE_RATE_44100  = 44100,    /**< 44.1K采样率 */
    AUD_SAMPLE_RATE_48000  = 48000,    /**< 48K采样率 */
    AUD_SAMPLE_RATE_64000  = 64000,    /**< 64K采样率 */
    AUD_SAMPLE_RATE_96000  = 96000,    /**< 96K采样率 */
    AUD_SAMPLE_RATE_INVALID,           /**< 无效采样率 */
} AudioSampleRate;

/**
* @brief 对齐结构定义，包含宽高的对齐
 */
typedef struct {
    int32_t widthAlignment;  /**< 宽的对齐值 */
    int32_t heightAlignment; /**< 高的对齐值 */
} Alignment;

/**
 * @brief 矩形的定义
 */
typedef struct {
    int32_t width;  /**< 矩形的宽 */
    int32_t height; /**< 矩形的高 */
} Rect;

/**
 * @brief 取值范围的定义
 */
typedef struct {
    int32_t min; /**< 最小值 */
    int32_t max; /**< 最大值 */
} RangeValue;

/**
 * @brief 枚举播放能力
 */
typedef enum {
    CODEC_CAP_ADAPTIVE_PLAYBACK = 0x1,    /**< 自适应播放能力 */
    CODEC_CAP_SECURE_PLAYBACK = 0x2,      /**< 安全播放能力 */
    CODEC_CAP_TUNNEL_PLAYBACK = 0x4,      /**< 通道播放能力 */
    CODEC_CAP_MULTI_PLANE = 0x10000,      /**< 视频图像平面/音频通道平面能力 */
} CodecCapsMask;

/**
 * @brief 枚举编解码处理模式
 */
typedef enum {
    PROCESS_BLOCKING_INPUT_BUFFER     = 0X1,      /**< 同步模式输入buffer */
    PROCESS_BLOCKING_OUTPUT_BUFFER    = 0X2,      /**< 同步模式输出buffer */
    PROCESS_BLOCKING_CONTROL_FLOW     = 0X4,      /**< 同步模式控制流 */
    PROCESS_NONBLOCKING_INPUT_BUFFER  = 0X100,    /**< 异步模式输入buffer */
    PROCESS_NONBLOCKING_OUTPUT_BUFFER = 0X200,    /**< 异步模式输出buffer */
    PROCESS_NONBLOCKING_CONTROL_FLOW  = 0X400,    /**< 异步模式控制流 */
} CodecProcessMode;

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* CODEC_COMMON_TYPE_H */
/** @} */