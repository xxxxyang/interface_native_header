/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Codec模块接口定义。
 *
 * Codec模块涉及自定义类型、音视频编解码组件初始化、参数设置、数据的轮转和控制等。
 *
 * @since 3.1
 * @version 2.0
 */

/**
 * @file codec_component_manager.h
 *
 * @brief 主要包括Codec组件管理类接口。
 *
 * Codec模块获取组件编解码能力集、创建组件和销毁组件等接口定义。
 *
 * @since 3.1
 * @version 2.0
 */

#ifndef CODEC_COMPONENT_MANAGER_H
#define CODEC_COMPONENT_MANAGER_H

#include "codec_component_if.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Codec组件管理类接口定义。
 *
 * 主要提供以下功能:
 * 获取Codec编解码组件数量以及编解码能力集表；
 * 创建/销毁Codec组件。
 */
struct CodecComponentManager {
    /**
     * @brief 获取Codec编解码组件数量。
     *
     * 通过此接口获取Codec编解码组件数量, 用来获取全部编解码能力集。
     *
     * @return Codec编解码组件数量。
     *
     * @since 3.1
     */
    int32_t (*GetComponentNum)();

    /**
     * @brief 获取编解码能力集表。
     *
     * 用户可通过此接口了解Codec模块提供了哪些编解码能力, 对应的能力体现在{@link CodecCompCapability}结构体。
     *
     * @param capList 输出参数, 返回全部组件的能力集表{@link CodecCompCapability}。
     * @param count 输入参数, 编解码组件数量, 由{@link GetComponentNum}获得。
     *
     * @return HDF_SUCCESS 表示获取能力集表成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效, 获取能力集表失败。
     * @return HDF_ERR_INVALID_OBJECT 表示对象无效, 获取能力集表失败。
     * @return HDF_ERR_MALLOC_FAIL 表示申请内存失败, 获取能力集表失败。
     *
     * @since 3.1
     */
    int32_t (*GetComponentCapabilityList)(CodecCompCapability *capList, int32_t count);

    /**
     * @brief 创建Codec组件实例。
     *
     * 根据组件名称创建Codec组件实例。
     *
     * @param component 输出参数, 指向Codec组件的指针。
     * @param compName 输入参数, 组件名称。
     * @param appData 输入参数, 指向应用程序定义的值的指针, 该值将在回调期间返回。
     * @param appDataSize 输入参数, appData字节数。
     * @param callbacks 输入参数, 回调接口, 指向OMX_CALLBACKTYPE结构的指针, 详见{@link CodecCallbackType}。
     *
     * @return HDF_SUCCESS 表示创建组件成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效, 创建组件失败。
     * @return HDF_ERR_INVALID_OBJECT 表示对象无效, 创建组件失败。
     * @return HDF_ERR_MALLOC_FAIL 表示申请内存失败, 创建组件失败。
     *
     * @since 3.1
     */
    int32_t (*CreateComponent)(struct CodecComponentType **component, char *compName, void *appData,
        int32_t appDataSize, struct CodecCallbackType *callbacks);

    /**
     * @brief 销毁组件实例。
     *
     * 销毁指定的Codec组件。
     *
     * @param component 输入参数, 需要销毁的Codec组件。
     *
     * @return HDF_SUCCESS 表示销毁组件成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效, 销毁组件失败。
     * @return HDF_ERR_INVALID_OBJECT 表示对象无效, 销毁组件失败。
     * @return HDF_ERR_MALLOC_FAIL 表示申请内存失败, 销毁组件失败。
     *
     * @since 3.1
     */
    int32_t (*DestoryComponent)(struct CodecComponentType *component);
};

/**
 * @brief 实例化CodecComponentManager对象。
 *
 * @return 实例化的CodecComponentManager对象。
 *
 * @since 3.1
 */
struct CodecComponentManager *GetCodecComponentManager(void);

/**
 * @brief 释放CodecComponentManager对象。
 *
 * @since 3.1
 */
void CodecComponentManagerRelease(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* CODEC_COMPONENT_MANAGER_H */
/** @} */