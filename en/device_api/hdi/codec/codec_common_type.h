/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Defines APIs related to the Codec module.
 *
 * The Codec module provides APIs for initializing the custom data and audio and video codecs,
 * setting codec parameters, and controlling and transferring data.
 *
 * @since 3.1
 * @version 2.0
 */

/**
 * @file codec_common_type.h
 *
 * @brief Declares custom data types used in the Codec module APIs, including the codec types,
 * audio and video parameters, and buffers.
 *
 * @since 3.1
 * @version 2.0
 */

#ifndef CODEC_COMMON_TYPE_H
#define CODEC_COMMON_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Enumerates the codec types.
 */
typedef enum {
    VIDEO_DECODER,     /**< Video decoder */
    VIDEO_ENCODER,     /**< Video encoder */
    AUDIO_DECODER,     /**< Audio decoder */
    AUDIO_ENCODER,     /**< Audio encoder */
    INVALID_TYPE,      /**< Invalid type */
} CodecType;

/**
 * @brief Enumerates the codec profiles.
 */
typedef enum {
    INVALID_PROFILE = 0,               /**< Invalid profile */
    AAC_LC_PROFILE = 0x1000,           /**< AAC-Low Complex */
    AAC_MAIN_PROFILE,                  /**< AAC-Main */
    AAC_HE_V1_PROFILE,                 /**< HEAAC, AAC+, or AACPlusV1 */
    AAC_HE_V2_PROFILE,                 /**< AAC++ or AACPlusV2 */
    AAC_LD_PROFILE,                    /**< AAC-Low Delay */
    AAC_ELD_PROFILE,                   /**< AAC-Enhanced Low Delay */
    AVC_BASELINE_PROFILE = 0x2000,     /**< H.264 Baseline */
    AVC_MAIN_PROFILE,                  /**< H.264 Main */
    AVC_HIGH_PROFILE,                  /**< H.264 High */
    HEVC_MAIN_PROFILE = 0x3000,        /**< H.265 Main */
    HEVC_MAIN_10_PROFILE,              /**< H.265 Main 10bit */
} Profile;

/**
 * @brief Enumerates the audio sampling rates.
 */
typedef enum {
    AUD_SAMPLE_RATE_8000   = 8000,     /**< 8 kHz */
    AUD_SAMPLE_RATE_12000  = 12000,    /**< 12 kHz */
    AUD_SAMPLE_RATE_11025  = 11025,    /**< 11.025 kHz */
    AUD_SAMPLE_RATE_16000  = 16000,    /**< 16 kHz */
    AUD_SAMPLE_RATE_22050  = 22050,    /**< 22.050 kHz */
    AUD_SAMPLE_RATE_24000  = 24000,    /**< 24 kHz */
    AUD_SAMPLE_RATE_32000  = 32000,    /**< 32 kHz */
    AUD_SAMPLE_RATE_44100  = 44100,    /**< 44.1 kHz */
    AUD_SAMPLE_RATE_48000  = 48000,    /**< 48 kHz */
    AUD_SAMPLE_RATE_64000  = 64000,    /**< 64 kHz */
    AUD_SAMPLE_RATE_96000  = 96000,    /**< 96 kHz */
    AUD_SAMPLE_RATE_INVALID,           /**< Invalid sampling rate */
} AudioSampleRate;

/**
* @brief Defines the alignment.
 */
typedef struct {
    int32_t widthAlignment; /**< Value to align with the width */
    int32_t heightAlignment; /**< Value to align with the height */
} Alignment;

/**
 * @brief Defines a rectangle.
 */
typedef struct {
    int32_t width;  /**< Width of the rectangle */
    int32_t height; /**< Height of the rectangle */
} Rect;

/**
 * @brief Defines a value range.
 */
typedef struct {
    int32_t min; /**< Minimum value */
    int32_t max; /**< Maximum value */
} RangeValue;

/**
 * @brief Enumerates the playback capabilities.
 */
typedef enum {
    CODEC_CAP_ADAPTIVE_PLAYBACK = 0x1, /**< Adaptive playback */
    CODEC_CAP_SECURE_PLAYBACK = 0x2,   /**< Secure playback */
    CODEC_CAP_TUNNEL_PLAYBACK = 0x4,   /**< Tunnel playback */
    CODEC_CAP_MULTI_PLANE = 0x10000,   /**< Multi-plane (video image plane and audio tunnel plane) playback */
} CodecCapsMask;

/**
 * @brief Enumerates the codec processing modes.
 */
typedef enum {
    PROCESS_BLOCKING_INPUT_BUFFER     = 0X1,    /**< Input buffer in sync mode */
    PROCESS_BLOCKING_OUTPUT_BUFFER    = 0X2,    /**< Output buffer in sync mode */
    PROCESS_BLOCKING_CONTROL_FLOW     = 0X4,    /**< Control flow in sync mode */
    PROCESS_NONBLOCKING_INPUT_BUFFER  = 0X100,  /**< Input buffer in async mode */
    PROCESS_NONBLOCKING_OUTPUT_BUFFER = 0X200,  /**< Output buffer in async mode */
    PROCESS_NONBLOCKING_CONTROL_FLOW  = 0X400,  /**< Control flow in async mode */
} CodecProcessMode;

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* CODEC_COMMON_TYPE_H */
/** @} */