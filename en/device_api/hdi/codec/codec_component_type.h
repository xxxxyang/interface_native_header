/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Defines APIs related to the Codec module.
 *
 * The Codec module provides APIs for initializing the custom data and audio and video codecs,
 * setting codec parameters, and controlling and transferring data.
 *
 * @since 3.1
 * @version 2.0
 */

/**
 * @file codec_component_type.h
 *
 * @brief Declares custom data types used in the Codec module APIs, including the codec types,
 * audio and video parameters, and buffers.
 *
 * @since 3.1
 * @version 2.0
 */

#ifndef CODEC_COMPONENT_TYPE_H
#define CODEC_COMPONENT_TYPE_H

#include <stdint.h>
#include <stdbool.h>
#include "OMX_Types.h"
#include "OMX_Index.h"
#include "codec_common_type.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Defines the maximum value of the sampling format.
 */
#define SAMPLE_FMT_NUM 32

/**
 * @brief Defines the length of UUID.
 */
#define UUID_LENGTH 128

/**
 * @brief Enumerates the types of audio and video encoding/decoding components.
 */
typedef enum {
    MEDIA_ROLETYPE_IMAGE_JPEG = 0,           /**< JPEG image */
    MEDIA_ROLETYPE_VIDEO_AVC,                /**< H.264 video */
    MEDIA_ROLETYPE_VIDEO_HEVC,               /**< H.265 video */
    MEDIA_ROLETYPE_AUDIO_FIRST = 0x10000,    /**< Audio codec */
    MEDIA_ROLETYPE_AUDIO_AAC = 0x10000,      /**< Advanced Audio Coding (AAC) */
    MEDIA_ROLETYPE_AUDIO_G711A,              /**< G711A audio */
    MEDIA_ROLETYPE_AUDIO_G711U,              /**< G711U audio */
    MEDIA_ROLETYPE_AUDIO_G726,               /**< G726 audio */
    MEDIA_ROLETYPE_AUDIO_PCM,                /**< Pulse-Code Modulation (PCM) audio */
    MEDIA_ROLETYPE_AUDIO_MP3,                /**< MP3 */
    MEDIA_ROLETYPE_INVALID,                  /**< Invalid type */
} AvCodecRole;

/**
 * @brief Enumerate the audio sampling formats.
 *
 * For the planar sampling format, the data of each channel is independently stored in data.
 * For the packed sampling format, only the first data is used, and the data of each channel is interleaved.
 */
typedef enum {
    AUDIO_SAMPLE_FMT_U8,       /**< Unsigned 8 bits, packed */
    AUDIO_SAMPLE_FMT_S16,      /**< Signed 16 bits, packed */
    AUDIO_SAMPLE_FMT_S32,      /**< Signed 32 bits, packed */
    AUDIO_SAMPLE_FMT_FLOAT,    /**< Float, packed */
    AUDIO_SAMPLE_FMT_DOUBLE,   /**< Double, packed */
    AUDIO_SAMPLE_FMT_U8P,      /**< Unsigned 8 bits, planar */
    AUDIO_SAMPLE_FMT_S16P,     /**< Signed 16 bits, planar */
    AUDIO_SAMPLE_FMT_S32P,     /**< Signed 32 bits, planar */
    AUDIO_SAMPLE_FMT_FLOATP,   /**< Float, planar */
    AUDIO_SAMPLE_FMT_DOUBLEP,  /**< Double, planar */
    AUDIO_SAMPLE_FMT_INVALID,  /**< Invalid sampling format */
} AudioSampleFormat;

/**
 * @brief Size of the supported pixel format array.
 */
#define PIX_FORMAT_NUM 16

/**
 * @brief Defines the video encoding and decoding capabilities.
 */
typedef struct {
    Rect minSize;                            /**< Minimum resolution supported. */
    Rect maxSize;                            /**< Maximum resolution supported. */
    Alignment whAlignment;                   /**< Values to align with the width and height. */
    RangeValue blockCount;                   /**< Number of blocks supported. */
    RangeValue blocksPerSecond;              /**< Number of blocks processed per second. */
    Rect blockSize;                          /**< Block size supported. */
    int32_t supportPixFmts[PIX_FORMAT_NUM];  /**< Supported pixel format. For details,
                                                  see {@link OMX_COLOR_FORMATTYPE}. */
} VideoPortCap;

/**
 * @brief Size of the audio sampling format array supported.
 */
#define SAMPLE_FORMAT_NUM 12

/**
 * @brief Size of the audio sampling rate array supported.
 */
#define SAMPLE_RATE_NUM 16

/**
 * @brief Size of the audio channel array supported.
 */
#define CHANNEL_NUM 16

/**
 * @brief Defines the audio encoding and decoding capabilities.
 */
typedef struct {
    int32_t sampleFormats[SAMPLE_FMT_NUM]; /**< Supported audio sampling formats. For details,
                                                see {@link AudioSampleFormat}. */
    int32_t sampleRate[SAMPLE_RATE_NUM];   /**< Supported audio sampling rates. For details,
                                                see {@link AudioSampleRate}. */
    int32_t channelLayouts[CHANNEL_NUM];   /**< Supported audio channel layouts. */
    int32_t channelCount[CHANNEL_NUM];     /**< Supported audio channels. */
} AudioPortCap;

/**
 * @brief Defines the audio and video encoding and decoding capabilities.
 */
typedef union {
    VideoPortCap video;               /**< Video encoding and decoding capabilities */
    AudioPortCap audio;               /**< Audio encoding and decoding capabilities */
} PortCap;

/**
 * @brief Size of the component name.
 */
#define NAME_LENGTH 32

/**
 * @brief Size of the profile array supported.
 */
#define PROFILE_NUM 256

/**
 * @brief Defines the codec capabilities.
 */
typedef struct {
    AvCodecRole role;                     /**< Media type. */
    CodecType type;                       /**< Codec type. */
    char compName[NAME_LENGTH];           /**< Codec component name. */
    int32_t supportProfiles[PROFILE_NUM]; /**< Supported profiles. For details, see {@link Profile}. */
    int32_t maxInst;                      /**< Maximum instance. */
    bool isSoftwareCodec;                 /**< Whether it is software codec or hardware codec. */
    int32_t processModeMask;              /**< Codec processing mode mask. For details,
                                               see {@link CodecProcessMode}. */
    uint32_t capsMask;                    /**< Codec playback capability mask. For details,
                                               see {@link CodecCapsMask}. */
    RangeValue bitRate;                   /**< Supported bit rate range. */
    PortCap port;                         /**< Supported audio and video encoding/decoding capabilities. */
} CodecCompCapability;

/**
 * @brief Enumerates the buffer types.
 */
enum CodecBufferType {
    CODEC_BUFFER_TYPE_INVALID = 0,           /**< Invalid buffer type. */
    CODEC_BUFFER_TYPE_VIRTUAL_ADDR = 0x1,    /**< Virtual address type. */
    CODEC_BUFFER_TYPE_AVSHARE_MEM_FD = 0x2,  /**< Shared memory. */
    CODEC_BUFFER_TYPE_HANDLE = 0x4,          /**< Handle. */
    CODEC_BUFFER_TYPE_DYNAMIC_HANDLE = 0x8,  /**< Dynamic handle. */
};

/**
 * @brief Enumerate the shared memory types.
 */
enum ShareMemTypes {
    READ_WRITE_TYPE = 0x1,    /**< Readable and writable shared memory */
    READ_ONLY_TYPE = 0x2,     /**< Readable shared memory */
};

/**
 * @brief Defines the codec buffer information.
 */
struct OmxCodecBuffer {
    uint32_t bufferId;               /**< Buffer ID. */
    uint32_t size;                   /**< Size of the structure. */
    union OMX_VERSIONTYPE version;   /**< Component version. */
    enum CodecBufferType bufferType; /**< Codec buffer type. */
    uint8_t *buffer;                 /**< Buffer used for encoding or decoding. */
    uint32_t bufferLen;              /**< Size of the buffer. */
    uint32_t allocLen;               /**< Size of the buffer allocated. */
    uint32_t filledLen;              /**< Size of the buffer filled. */
    uint32_t offset;                 /**< Offset to the start position of the valid data in the buffer. */
    int32_t fenceFd;                 /**< Fence file descriptor used to signal when the input or
                                          output buffer is ready to consume. */
    enum ShareMemTypes type;         /**< Shared memory type. */
    int64_t pts;                     /**< Timestamp. */
    uint32_t flag;                   /**< Flag. */
};

/**
 * @brief Enumerates the extended codec indexes.
 */
enum OmxIndexCodecExType {
    OMX_IndexExtBufferTypeStartUnused = OMX_IndexKhronosExtensions + 0x00a00000, /**< Extended BufferType index */
    OMX_IndexParamSupportBufferType,                                             /**< SupportBuffer */
    OMX_IndexParamUseBufferType,                                                 /**< UseBuffer */
    OMX_IndexParamGetBufferHandleUsage,                                          /**< GetBufferHandleUsage */
};

/**
 * @brief Enumerates the extended codec coding types.
 */
enum OmxVideoExType {
    OMX_VIDEO_CodingHEVC = 11,  /**< HEVC coding type */
};

/**
 * @brief Defines the component version information.
 */
struct CompVerInfo {
    char compName[NAME_LENGTH];         /**< The name of the component. */
    uint8_t compUUID[UUID_LENGTH];      /**< The UUID of the component. */
    union OMX_VERSIONTYPE compVersion;  /**< The version of the component. */
    union OMX_VERSIONTYPE specVersion;  /**< The spec version of the component. */
};

/**
 * @brief Defines the event information.
 */
struct EventInfo {
    int8_t *appData;                /**< The pointer to the upper-layer instance passed to the callback. */
    uint32_t appDataLen;            /**< The length of appData, in bytes. */
    uint32_t data1;                 /**< Data 1 carried in the event. */
    uint32_t data2;                 /**< Data 2 carried in the event. */
    int8_t *eventData;              /**< The pointer of data carried in the event. */
    uint32_t eventDataLen;          /**< The length of eventData, in bytes. */
};

/**
 * @brief Defines the <b>SupportBuffer</b>.
 */
struct SupportBufferType {
    uint32_t size;                                          /**< Size of the structure */
    union OMX_VERSIONTYPE version;                          /**< Component version */
    uint32_t portIndex;                                     /**< Port index */
    uint32_t bufferTypes;                                   /**< Supported buffer types */
};

/**
 * @brief Define the <b>UseBuffer</b>.
 */
struct UseBufferType {
    uint32_t size;                                         /**< Size of the structure */
    union OMX_VERSIONTYPE version;                         /**< Component version */
    uint32_t portIndex;                                    /**< Port index */
    uint32_t bufferType;                                   /**< Buffer type */
};

/**
 * @brief Defines the <b>BufferHandleUsage</b>.
 */
struct GetBufferHandleUsageParams {
    uint32_t size;                                         /**< Size of the structure */
    union OMX_VERSIONTYPE version;                         /**< Component version */
    uint32_t portIndex;                                    /**< Port index */
    uint32_t usage;                                        /**< Usage */
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* CODEC_COMPONENT_TYPE_H */
/** @} */
